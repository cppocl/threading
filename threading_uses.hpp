/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCLCPP_GUARD_THREADING_USES_HPP
#define OCLCPP_GUARD_THREADING_USES_HPP

#include "threading.hpp"

using ocl::ASyncThread;
using ocl::FileQueueThread;
using ocl::FileQueueThreads;
using ocl::IFileFunctor;
using ocl::IEvent;
using ocl::IFunctor;
using ocl::IThread;
using ocl::IThreads;
using ocl::MainThread;
using ocl::EventThread;
using ocl::QueueThread;
using ocl::QueueThreads;
using ocl::Thread;
using ocl::ThreadApp;
using ocl::TimerThread;
using ocl::ThreadSafeQueue;
using ocl::ThreadSafeLoggers;
using ocl::ThreadSafeLocalTime;
using ocl::PrintThreadSafeNumber;
using ocl::PrintThreadSafeString;
using ocl::PrintThreadSafeDateTime;
using ocl::PrintThreadSafeDirEntry;

#endif // OCLCPP_GUARD_THREADING_USES_HPP
