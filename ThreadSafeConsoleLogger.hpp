/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_THREADSAFECONSOLELOGGER_HPP
#define CPPOCL_GUARD_THREADSAFECONSOLELOGGER_HPP

#include "ILogger.hpp"
#include <iostream>
#include <mutex>

namespace ocl
{

class ThreadSafeConsoleLogger : public ILogger
{
public:
    ThreadSafeConsoleLogger(bool error_logger = false)
        : m_error_logger(error_logger)
    {
    }

    ~ThreadSafeConsoleLogger() override = default;

    ThreadSafeConsoleLogger(ThreadSafeConsoleLogger const&) = delete;
    ThreadSafeConsoleLogger(ThreadSafeConsoleLogger&&) = delete;
    ThreadSafeConsoleLogger& operator=(ThreadSafeConsoleLogger const&) = delete;
    ThreadSafeConsoleLogger& operator=(ThreadSafeConsoleLogger&&) = delete;

    void Write(std::string const& message) override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_error_logger)
            std::cerr << message << std::endl;
        else
            std::cout << message << std::endl;
    }

private:
    std::mutex m_mutex;
    bool m_error_logger;
};

} // namespace ocl

#endif // CPPOCL_GUARD_THREADSAFECONSOLELOGGER_HPP
