/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_IEVENT_HPP
#define CPPOCL_GUARD_IEVENT_HPP

#include <memory>
#include <functional>

namespace ocl
{

/// Unique event used by EventThread for looking up event and executing a functor.
class IEvent
{
public:
    IEvent() = default;
    virtual ~IEvent() = default;

    /// Create a copy of this event.
    virtual IEvent* Clone() = 0;

    /// Compare two events to see if they match.
    /// Return true when less than other event.
    virtual bool IsLess(IEvent*) const = 0;

    /// When the event type is unique, such as an abort or a terminate event, return true.
    virtual bool IsUnique() const
    {
        return false;
    }
};

} // namespace ocl

#endif // CPPOCL_GUARD_IEVENT_HPP
