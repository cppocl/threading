/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_EVENTTHREAD_HPP
#define CPPOCL_GUARD_EVENTTHREAD_HPP

#include "IThread.hpp"
#include "IFunctor.hpp"
#include "ThreadSafeEventQueue.hpp"
#include "utility/PlatformThreadCreate.hpp"
#include <map>
#include <list>
#include <mutex>
#include <atomic>
#include <memory>

/// A thread loop that receives events and sends notifiers.
namespace ocl
{

/// Register functors with the event thread,
/// notifying when a condition is met for each functor.
class EventThread final : public IThread
{
public:
    /// Construct a named EventThread object.
    EventThread(std::string const& name = "EventThread")
        : m_name(name)
    {
        m_platform_thread = PlatformThreadCreate();
    }

    /// Move the EventThread object.
    /// There is the potential for std::queue constructor to throw an exception,
    /// is it does not specify noexcept.
    EventThread(EventThread&& queue_thread) noexcept
    {
        try
        {
            queue_thread.m_mutex.lock();
            m_name = std::move(queue_thread.m_name);
            m_abort = queue_thread.m_abort.load();
            m_terminate = queue_thread.m_terminate.load();
            m_paused = queue_thread.m_paused.load();
            m_started = queue_thread.m_started.load();
            m_running = queue_thread.m_running.load();
            m_queue = std::move(queue_thread.m_queue);
            m_event_map = std::move(queue_thread.m_event_map);
            queue_thread.m_mutex.unlock();
        }
        catch (std::exception&)
        {
        }
    }

    EventThread(EventThread const&) = delete;
    EventThread& operator=(EventThread const&) = delete;
    EventThread& operator=(EventThread&&) = delete;

    ~EventThread() override
    {
    }

    /// Register a functor for a specific event.
    /// Each time an event is later added to the queue,
    /// the associated functor will be executed for the event.
    void Register(std::unique_ptr<IEvent> event_ptr, IFunctor* functor)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        RegisterThreadUnsafe(std::move(event_ptr), functor_pointer_type(functor));
    }

    /// Register a functor for a specific event.
    /// Each time an event is later added to the queue,
    /// the associated functor will be executed for the event.
    void Register(IEvent* event_ptr, IFunctor* functor)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        RegisterThreadUnsafe(event_pointer_type(event_ptr), functor_pointer_type(functor));
    }

    /// UUnregister an event and the associated functor.
    void Unregister(std::unique_ptr<IEvent> event_ptr)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        if (!m_abort && !m_terminate)
        {
            auto iter = m_event_map.find(event_ptr);
            if (iter != m_event_map.end())
                m_event_map.erase(iter);
        }
    }

    /// Add an event to the event queue.
    /// The registered functor for the event will be executed once the event is reached.
    void Add(IEvent* event_ptr)
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            if (!m_abort && !m_terminate)
                m_queue.Push(event_ptr);
            else
                delete event_ptr;
        }

        m_cv.notify_one();
    }

// Implement IThread interface.
public:

    /// This is always a child thread.
    bool IsMainThread() const noexcept override
    {
        return false;
    }

    /// Get the thread identifier.
    virtual id_type GetId() const noexcept
    {
        return m_thread.get_id();
    }

    /// Get the thread or nullptr if the thread is not running.
    std::thread* GetThread() noexcept override
    {
        return &m_thread;
    }

    /// Get the thread or nullptr if the thread is not running for const member functions.
    std::thread const* GetThread() const noexcept override
    {
        return &m_thread;
    }

    /// Get the name of the thread.
    std::string const& GetName() const noexcept override
    {
        return m_name;
    }

    /// Start the thread.
    virtual void Start()
    {
        m_thread = std::thread(&ThreadFunction, std::ref(*this));
        m_platform_thread->SetThreadName(m_thread, m_name.c_str());
        m_started = true;
    }

    /// Return when the threaded function is a loop.
    bool IsThreadLoop() const noexcept override
    {
        return true;
    }

    /// Return true when the thread is terminating.
    bool IsTerminating() const override
    {
        return m_terminate;
    }

    /// Terminate the thread, waiting until existing items in the queue are complete.
    void Terminate() override
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_terminate = true;
        }

        m_cv.notify_one();
    }

    /// Return true when the thread is aborting.
    bool IsAborting() const override
    {
        return m_abort;
    }

    /// Abort the thread, discarding any remaining items in the queue.
    void Abort() override
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_abort = true;
        }

        m_cv.notify_one();
    }

    /// When not terminating and not aborting this will return true.
    bool IsContinuing() const override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return !m_abort && !m_terminate;
    }

    bool IsJoinable() const noexcept override
    {
        return m_thread.joinable();
    }

    /// Join the thread back to the parent thread.
    void Join() override
    {
        m_thread.join();
        m_started = false;
    }

    /// Tell the thread to pause.
    void Pause() override
    {
        m_paused = true;
    }

    /// Tell the thread to resume, if paused.
    void Resume() override
    {
        m_paused = false;
    }

    /// Return true is the thread is paused.
    bool IsPaused() const noexcept override
    {
        return m_paused;
    }

    /// When the thread function or thread loop is running, this will be true.
    bool IsRunning() const noexcept override
    {
        return m_running;
    }

    /// When the thread has started and has not been joined, this will be true.
    bool IsActive() const noexcept override
    {
        return m_started;
    }

    /// Get the size of the queue.
    std::size_t GetSize() const
    {
        return m_queue.GetSize();
    }

private:
    void WaitToContinue()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_cv.wait(lock, [&] { return m_abort || m_terminate || !m_queue.IsEmpty(); });
    }

    /// The queue function that keeps looping until aborted or terminated.
    void ThreadLoop()
    {
        std::unique_ptr<IFunctor> functor;
        std::unique_ptr<IEvent> event_ptr;
        m_running = true;

        while (!m_abort && !m_terminate)
        {
            WaitToContinue();

            event_ptr = std::move(m_queue.Pop());
            if (event_ptr)
                ExecuteFunctorForEvent(std::move(event_ptr));
        }

        if (m_terminate)
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            while ((event_ptr = std::move(m_queue.Pop())))
                ExecuteFunctorForEvent(std::move(event_ptr));
        }

        m_running = false;
    }

private:
    /// Provide functor for comparing two events.
    struct EventLess
    {
        EventLess() = default;

        bool operator ()(std::unique_ptr<IEvent> const& left, std::unique_ptr<IEvent> const& right) const
        {
            return left->IsLess(right.get());
        }
    };

    /// Register a functor for a specific event.
    /// Each time an event is later added to the queue,
    /// the associated functor will be executed for the event.
    void RegisterThreadUnsafe(std::unique_ptr<IEvent> event_ptr, std::unique_ptr <IFunctor> functor)
    {
        if (!m_abort && !m_terminate)
        {
            auto iter = m_event_map.find(event_ptr);
            if (iter != m_event_map.end())
            {
                std::list<functor_pointer_type>& l = iter->second;
                l.push_back(std::move(functor));
            }
            else
            {
                std::list<functor_pointer_type> l;
                l.push_back(std::move(functor));
                event_pair_type p(std::move(event_ptr), std::move(l));
                m_event_map.insert(std::move(p));
            }
        }
    }

    /// Find the functors for the event and execute all the functors.
    /// Return false if the event was not found.
    bool ExecuteFunctorForEvent(std::unique_ptr<IEvent> event_ptr)
    {
        auto iter = m_event_map.find(event_ptr);
        bool success = iter != m_event_map.end();

        if (success)
        {
            std::list<functor_pointer_type>& functor_list = iter->second;
            std::list<functor_pointer_type>::iterator functor_iter = functor_list.begin();
            while (functor_iter != functor_list.end())
            {
                functor_pointer_type& func = *functor_iter;
                func->Execute();
                ++functor_iter;
            }
        }

        return success;
    }

    /// The function running in the thread.
    static void ThreadFunction(EventThread& queue_thread)
    {
        queue_thread.ThreadLoop();
    }

private:

    /// Types for key and value stored within m_event_map.
    typedef std::unique_ptr<IEvent>   event_pointer_type;
    typedef std::unique_ptr<IFunctor> functor_pointer_type;

    /// Defined type for inserting new element into m_event_map;
    typedef std::pair<event_pointer_type, std::list<functor_pointer_type>> event_pair_type;

    /// Name of thread.
    std::string m_name;

    /// Thread state variables.
    std::atomic_bool m_abort = false;     // True after Abort function is called.
    std::atomic_bool m_terminate = false; // True after Terminate function called.
    std::atomic_bool m_paused = false;    // True when user pauses thread.
    std::atomic_bool m_started = false;   // True when the Start function is called and false after join.
    std::atomic_bool m_running = false;   // True while the thread loop is running.

    /// Queue of events to be popped and execute a functor related to the event.
    ThreadSafeEventQueue m_queue;

    /// Map each unique event to the functor that will be executed.
    std::map<std::unique_ptr<IEvent>, std::list<std::unique_ptr<IFunctor>>, EventLess> m_event_map;

    /// General use mutex for protecting access to multiple member variables.
    mutable std::mutex m_mutex;

    /// Condition variable used to release thread resources when not active.
    std::condition_variable m_cv;

    /// Must be defined last for other variables to be usable within the thread.
    std::thread m_thread;

    // Platform specific extensions supported through PlatformThread class.
    std::unique_ptr<PlatformThread> m_platform_thread;
};

} //namespace ocl

#endif // CPPOCL_GUARD_EVENTTHREAD_HPP
