/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_TIMERTHREAD_HPP
#define CPPOCL_GUARD_TIMERTHREAD_HPP

#include "utility/PlatformThreadCreate.hpp"
#include <chrono>
#include <thread>
#include <string>
#include <cstddef>

namespace ocl
{

class TimerThread : public IThread
{
public:
    TimerThread(std::uint64_t delay_ms, IFunctor* functor, std::string const& name = "TimerThread")
        : m_name(name)
        , m_delay_ms(delay_ms)
        , m_functor(std::unique_ptr<IFunctor>(functor))
    {
        m_platform_thread = PlatformThreadCreate();
    }

    TimerThread(TimerThread&& timer_thread) noexcept
    {
        try
        {
            std::lock_guard<std::mutex> lock(timer_thread.m_mutex);
            m_name = std::move(timer_thread.m_name);
            m_delay_ms = timer_thread.m_delay_ms.load();
            m_functor = std::move(timer_thread.m_functor);
        }
        catch (std::exception&)
        {
        }
    }

    virtual ~TimerThread()
    {
    }

    TimerThread(TimerThread const&) = delete;
    TimerThread& operator=(TimerThread const&) = delete;
    TimerThread& operator=(TimerThread&&) = delete;

    /// Check if the thread is the main thread or child a thread.
    /// The main thread is the applications main thread from main.
    virtual bool IsMainThread() const noexcept
    {
        return false;
    }

    /// Get the thread identifier.
    virtual id_type GetId() const noexcept
    {
        return m_thread.get_id();
    }

    /// Get the thread or nullptr if the thread is not running.
    virtual std::thread* GetThread() noexcept
    {
        return &m_thread;
    }

    /// Get the thread or nullptr if the thread is not running for const member functions.
    virtual std::thread const* GetThread() const noexcept
    {
        return &m_thread;
    }

    /// Get the name of the thread.
    virtual std::string const& GetName() const noexcept
    {
        return m_name;
    }

    /// Return when the threaded function is a loop.
    virtual bool IsThreadLoop() const noexcept
    {
        return true;
    }

    /// Start the thread, by running the thread function or thread loop.
    virtual void Start()
    {
        m_thread = std::thread(&ThreadFunction, std::ref(*this));
        m_platform_thread->SetThreadName(m_thread, m_name.c_str());
    }

    /// Return true when the thread is terminating.
    /// The current thread action or items on a thread queue will complete.
    bool IsTerminating() const override
    {
        return m_terminate;
    }

    /// Support the ability to terminate a thread, completing any remaining thread activity.
    /// Return true if Terminate is implemented and is handled, otherwise false.
    virtual void Terminate()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_terminate = true;
    }

    /// Return true when the thread is aborting.
    /// The current thread action will complete.
    bool IsAborting() const override
    {
        return m_abort;
    }

    /// Support the ability to abort a thread immediately, ignoring any remaining activities.
    /// Return true if Abort is implemented and is handled, otherwise false.
    virtual void Abort()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_abort = true;
    }

    /// When not terminating and not aborting this will return true.
    bool IsContinuing() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return !m_abort && !m_terminate;
    }

    /// Check if the thread can be joined, as it might already have been done.
    virtual bool IsJoinable() const
    {
        return m_thread.joinable();
    }

    /// Wait for the thread to complete.
    virtual void Join()
    {
        m_thread.join();
        m_started = false;
    }

    /// Tell the thread to pause.
    virtual void Pause()
    {
        m_paused = true;
    }

    /// Tell the thread to resume, if paused.
    virtual void Resume()
    {
        m_paused = true;
    }

    /// Return true is the thread is paused.
    virtual bool IsPaused() const noexcept
    {
        return m_paused;
    }

    /// When the thread function or thread loop is running, this will be true.
    virtual bool IsRunning() const noexcept
    {
        return m_running;
    }

    /// When the thread has started and has not been joined, this will be true.
    virtual bool IsActive() const noexcept
    {
        return m_started;
    }

    std::uint64_t GetDelay() const
    {
        return m_delay_ms;
    }

    void SetDelay(std::uint64_t delay_ms)
    {
        m_delay_ms = delay_ms;
    }

    /// The timer function running in a separate thread.
    void TimerFunction()
    {
        m_running = true;
        auto now(std::chrono::system_clock::now());
        auto last = now;

        // Required delay for timer, if the functor did not take any time to execute.
        std::chrono::milliseconds delay_ms(m_delay_ms);

        // Current delay is adjusted to compensate for the time the functor takes to execute.
        std::chrono::milliseconds curr_delay_ms = delay_ms;

        while (!m_abort && !m_terminate)
        {
            std::this_thread::sleep_for(curr_delay_ms);
            if (!m_paused)
                m_functor->Execute();

            last = now;
            now = std::chrono::system_clock::now();

            // Set new delay, removing the time taken to execute the functor.
            std::chrono::milliseconds diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - last);
            if (diff < delay_ms)
                curr_delay_ms = std::chrono::duration_cast<std::chrono::milliseconds>(delay_ms - diff);
            else
                curr_delay_ms = std::chrono::milliseconds(0);

            // Update the delay value used for calculations, as SetDelay can change it.
            delay_ms = std::chrono::milliseconds(m_delay_ms);
        }

        m_running = false;
    }

private:
    /// The function running in the thread.
    static void ThreadFunction(TimerThread& timer_thread)
    {
        timer_thread.TimerFunction();
    }

private:
    mutable std::mutex m_mutex;
    std::string m_name;
    std::atomic_uint64_t m_delay_ms;
    std::atomic_bool m_abort = false;     // True after Abort function is called.
    std::atomic_bool m_terminate = false; // True after Terminate function called.
    std::atomic_bool m_paused = false;    // True when user pauses thread.
    std::atomic_bool m_started = false;   // True when the Start function is called and false after join.
    std::atomic_bool m_running = false;   // True while the thread loop is running.
    std::unique_ptr<IFunctor> m_functor;

    // Ensure thread is always last, allowing access to members.
    std::thread m_thread;

    // Platform specific extensions supported through PlatformThread class.
    std::unique_ptr<PlatformThread> m_platform_thread;
};

} // namespace ocl

#endif // CPPOCL_GUARD_TIMERTHREAD_HPP

