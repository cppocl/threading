/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_EVENTS_NAMEDEVENT_HPP
#define CPPOCL_GUARD_EVENTS_NAMEDEVENT_HPP

#include <string>

namespace ocl
{

class NamedEvent : public IEvent
{
public:
    NamedEvent(std::string const& name)
        : m_name(name)
    {
    }

    NamedEvent() = delete;
    NamedEvent(NamedEvent const&) = default;
    NamedEvent(NamedEvent&&) = default;
    NamedEvent& operator =(NamedEvent const&) = delete;
    NamedEvent& operator =(NamedEvent&&) = delete;

    std::string const& GetName() const noexcept
    {
        return m_name;
    }

    virtual IEvent* Clone()
    {
        return new NamedEvent(m_name);
    }

    virtual bool IsLess(IEvent* event_ptr) const
    {
        NamedEvent* ev = dynamic_cast<NamedEvent*>(event_ptr);
        return ev && m_name.compare(ev->GetName()) < 0;
    }

private:
    std::string m_name;
};

} // namespace ocl

#endif // CPPOCL_GUARD_EVENTS_NAMEDEVENT_HPP
