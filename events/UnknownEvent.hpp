/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_EVENTS_UNKNOWNEVENT_HPP
#define CPPOCL_GUARD_EVENTS_UNKNOWNEVENT_HPP

#include <string>

namespace ocl
{

class UnknownEvent : public IEvent
{
public:
    UnknownEvent() = default;
    UnknownEvent(UnknownEvent const&) = default;
    UnknownEvent(UnknownEvent&&) = default;
    UnknownEvent& operator =(UnknownEvent const&) = delete;
    UnknownEvent& operator =(UnknownEvent&&) = delete;

    /// There will never be more than one unknown event, and so cannot be cloned.
    IEvent* Clone() override
    {
        return nullptr;
    }

    /// Always return true, as there will never be a need to compare.
    bool IsLess(IEvent*) const override
    {
        // There should only ever be one of these types of events.
        return true;
    }
};

} // namespace ocl

#endif // CPPOCL_GUARD_EVENTS_UNKNOWNEVENT_HPP
