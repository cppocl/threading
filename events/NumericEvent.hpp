/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_EVENTS_NUMERICEVENT_HPP
#define CPPOCL_GUARD_EVENTS_NUMERICEVENT_HPP

#include <string>

namespace ocl
{

template<typename NumericType = unsigned long>
class NumericEvent : public IEvent
{
public:
    NumericEvent(NumericType number)
        : m_number(number)
    {
    }

    NumericEvent() = delete;
    NumericEvent(NumericEvent const&) = default;
    NumericEvent(NumericEvent&&) = default;
    NumericEvent& operator =(NumericEvent const&) = delete;
    NumericEvent& operator =(NumericEvent&&) = delete;

    NumericType GetNumber() const noexcept
    {
        return NumericType;
    }

    virtual IEvent* Clone()
    {
        return new NumericEvent(m_number);
    }

    virtual bool IsLess(IEvent* event_ptr) const
    {
        NumericEvent* ev = dynamic_cast<NumericEvent*>(event_ptr);
        return ev && m_number < ev->GetNumber() < 0;
    }

private:
    NumericType m_number;

};

} // namespace ocl

#endif // CPPOCL_GUARD_EVENTS_NUMERICEVENT_HPP
