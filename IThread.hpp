/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_ITHREAD_HPP
#define CPPOCL_GUARD_ITHREAD_HPP

#include <string>
#include <thread>

namespace ocl
{

class IThread
{
public:
    typedef std::thread::id id_type;

public:
    IThread() = default;
    virtual ~IThread() {}

    IThread(IThread const&) = delete;
    IThread(IThread&&) = delete;
    IThread& operator=(IThread const&) = delete;
    IThread& operator=(IThread&&) = delete;

    /// Check if the thread is the main thread or child a thread.
    /// The main thread is the applications main thread from main.
    virtual bool IsMainThread() const = 0;

    /// Get the thread identifier.
    virtual id_type GetId() const = 0;

    /// Get the thread or nullptr if the thread is not running.
    virtual std::thread* GetThread() = 0;

    /// Get the thread or nullptr if the thread is not running for const member functions.
    virtual std::thread const* GetThread() const = 0;

    /// Get the name of the thread.
    virtual std::string const& GetName() const = 0;

    /// Return when the threaded function is a loop.
    virtual bool IsThreadLoop() const = 0;

    /// Start the thread, by running the thread function or thread loop.
    virtual void Start() = 0;

    /// Return true when the thread is terminating.
    /// The current thread action or items on a thread queue will complete.
    virtual bool IsTerminating() const = 0;

    /// Support the ability to terminate a thread, completing any remaining thread activity.
    /// Return true if Terminate is implemented and is handled, otherwise false.
    virtual void Terminate() = 0;

    /// Return true when the thread is aborting.
    /// The current thread action or current item on a thread queue will complete.
    /// Any other items on a thread queue will abort.
    virtual bool IsAborting() const = 0;

    /// Support the ability to abort a thread immediately, ignoring any remaining activities.
    /// Return true if Abort is implemented and is handled, otherwise false.
    virtual void Abort() = 0;

    /// When not terminating and not aborting this will return true.
    virtual bool IsContinuing() const = 0;

    /// Check if the thread can be joined, as it might already have been done.
    virtual bool IsJoinable() const = 0;

    /// Wait for the thread to complete.
    virtual void Join() = 0;

    /// Tell the thread to pause.
    virtual void Pause() = 0;

    /// Tell the thread to resume, if paused.
    virtual void Resume() = 0;

    /// Return true is the thread is paused.
    virtual bool IsPaused() const = 0;

    /// When the thread function or thread loop is running, this will be true.
    virtual bool IsRunning() const = 0;

    /// When the thread has started and has not been joined, this will be true.
    virtual bool IsActive() const = 0;
};

} // namespace ocl

#endif // CPPOCL_GUARD_ITHREAD_HPP
