/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_FILEQUEUETHREADS_HPP
#define CPPOCL_GUARD_FILEQUEUETHREADS_HPP

#include "IThreads.hpp"
#include "IFileFunctor.hpp"
#include "FileQueueThread.hpp"
#include <mutex>
#include <atomic>
#include <exception>
#include <condition_variable>
#include <memory>
#include <string>

namespace ocl
{

// Thread class containing two threads to push and pop from a thread-safe queue of IFileFunctor objects.
//
// Guarantees that IFileFunctor objects can be pushed onto a queue and immediately returned,
// without waiting for the queue to process existing items.
class FileQueueThreads final : public IThreads
{
public:

    /// Construct a named FileQueueThreads object.
    FileQueueThreads(IFileFunctor* functor,
                     std::size_t max_queues,
                     std::string const& name = "FileQueueThreads")

        : m_functor(functor)
        , m_max_queues(max_queues)
        , m_started(false)
        , m_name(name)
    {
        for (std::size_t pos = 0U; pos < max_queues; ++pos)
        {
            std::string thread_name = name + " " + std::to_string(pos + 1U);
            m_threads.push_back(std::unique_ptr<FileQueueThread>(
                new FileQueueThread(m_functor->Clone(m_empty_dir_entry), thread_name)));
        }
    }

    /// Move the FileQueueThreads object.
    /// There is the potential for std::queue constructor to throw an exception,
    /// is it does not specify noexcept.
    FileQueueThreads(FileQueueThreads&& file_queue_thread) noexcept
    {
        try
        {
            file_queue_thread.Pause();
            file_queue_thread.m_mutex.lock();
            m_name = std::move(file_queue_thread.m_name);
            m_max_queues = file_queue_thread.m_max_queues;
            file_queue_thread.m_mutex.unlock();
        }
        catch (std::exception&)
        {
        }
    }

    FileQueueThreads(FileQueueThreads const&) = delete;
    FileQueueThreads& operator=(FileQueueThreads const&) = delete;
    FileQueueThreads& operator=(FileQueueThreads&&) = delete;

    ~FileQueueThreads()
    {
    }

    /// Search the file system for files,
    /// cloning the functor supplied within the constructor for each matching file.
    void Search(std::filesystem::path const& search_path, bool recursive)
    {
        if (recursive)
            Iterate<std::filesystem::recursive_directory_iterator>(search_path);
        else
            Iterate<std::filesystem::directory_iterator>(search_path);
    }

/// Implement IThreads interface.
public:

    /// Get number of stored IThread objects.
    std::size_t GetSize() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return m_threads.size();
    }

    /// Set the number of threads.
    /// Threads to be removed will first be terminated or aborted.
    void SetSize(std::size_t new_size, bool terminate = true) override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        std::size_t size = m_threads.size();
        if (new_size > size)
            AddThreads(new_size - size);
        else if (new_size < size)
            RemoveThreads(size - new_size, terminate);
    }

/// Implement IThread interface.
public:

    /// This is always a child thread.
    bool IsMainThread() const noexcept override
    {
        return false;
    }

    /// Get the thread identifier for the first thread in the list of queue threads.
    virtual id_type GetId() const noexcept
    {
        id_type no_id;
        return m_threads.empty() ? no_id : m_threads[0]->GetId();
    }

    /// This function is not used for containers of threads.
    std::thread* GetThread() override
    {
        return nullptr;
    }

    /// This function is not used for containers of threads.
    std::thread const* GetThread() const override
    {
        return nullptr;
    }

    /// Get the name of the thread.
    std::string const& GetName() const noexcept override
    {
        return m_name;
    }

    /// Start the thread.
    virtual void Start()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread>& t : m_threads)
            t->Start();
        m_started = true;
    }

    /// Return when the threaded function is a loop.
    bool IsThreadLoop() const noexcept override
    {
        return true;
    }

    /// Return true when any thread is terminating.
    bool IsTerminating() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        bool terminating = true;

        for (std::unique_ptr<FileQueueThread> const& t : m_threads)
        {
            if (!t->IsTerminating())
            {
                terminating = false;
                break;
            }
        }

        return terminating;
    }

    /// Terminate the thread, waiting until existing items in the queue are complete.
    void Terminate() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread>& t : m_threads)
            t->Terminate();
    }

    /// Return true when all the threads are aborting.
    bool IsAborting() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        bool aborting = true;

        for (std::unique_ptr<FileQueueThread> const& t : m_threads)
        {
            if (!t->IsAborting())
            {
                aborting = false;
                break;
            }
        }

        return aborting;
    }

    /// Abort the thread, discarding any remaining items in the queue.
    void Abort() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread>& t : m_threads)
            t->Abort();
    }

    /// When not terminating and not aborting this will return true.
    bool IsContinuing() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        bool continuing = false;

        for (std::unique_ptr<FileQueueThread> const& t : m_threads)
        {
            if (!t->IsAborting() && !t->IsTerminating())
            {
                continuing = true;
                break;
            }
        }

        return continuing;
    }

    bool IsJoinable() const
    {
        bool joinable = true;

        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread> const& t : m_threads)
            joinable = t->IsJoinable();

        return joinable;
    }

    /// Join the thread back to the parent thread.
    void Join() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread>& t : m_threads)
            t->Join();
    }

    /// Tell the thread to pause.
    void Pause() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread>& t : m_threads)
            t->Pause();
    }

    /// Tell the thread to resume, if paused.
    void Resume() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread>& t : m_threads)
            t->Resume();
    }

    /// Return true is the thread is paused.
    bool IsPaused() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread> const& t : m_threads)
            if (t->IsPaused())
                return true;

        return false;
    }

    /// When the thread function or thread loop is running, this will be true.
    bool IsRunning() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread> const& t : m_threads)
            if (t->IsRunning())
                return true;

        return false;
    }

    /// When the thread has started and has not been joined, this will be true.
    bool IsActive() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<FileQueueThread> const& t : m_threads)
            if (t->IsActive())
                return true;

        return false;
    }

private:

    /// Add one of more new threads.
    void AddThreads(std::size_t count = 1)
    {
        std::size_t offset = m_threads.size() + 1U;

        for (std::size_t pos = 0U; pos < count; ++pos)
        {
            std::string thread_name = m_name + " " + std::to_string(pos + offset);

            std::unique_ptr<FileQueueThread> file_queue_thread(
                new FileQueueThread(m_functor->Clone(m_empty_dir_entry), thread_name));

            if (m_started)
                file_queue_thread->Start();

            m_threads.push_back(std::move(file_queue_thread));
        }
    }

    /// Remove one of more threads.
    void RemoveThreads(std::size_t count = 1, bool terminate = true)
    {
        if (count > m_threads.size())
            count = m_threads.size();
        if (count > 0)
        {
            std::size_t start = count < m_threads.size() ? m_threads.size() - count : 0;
            for (std::size_t index = start; index < m_threads.size(); ++index)
            {
                if (terminate)
                    m_threads[index]->Terminate();
                else
                    m_threads[index]->Abort();
            }
            m_threads.resize(m_threads.size() - count);
        }
    }

    /// Find the next index to add to the queues.
    std::size_t GetAddIndex() const
    {
        std::size_t index = 0;
        std::size_t smallest = ~static_cast<std::size_t>(0);

        for (std::size_t pos = 0; pos < m_threads.size(); ++pos)
        {
            std::unique_ptr<FileQueueThread> const& file_queue_thread = m_threads[pos];
            std::size_t size = file_queue_thread->GetSize();
            if (size < smallest)
            {
                index = pos;
                smallest = size;
            }
        }

        return index;
    }

    template<typename IteratorType>
    void Iterate(std::filesystem::path const& search_path)
    {
        for (auto const& dir_entry : IteratorType(search_path))
        {
            {
                std::unique_lock<std::mutex> lock(m_mutex);
                std::size_t add_index = GetAddIndex();
                std::unique_ptr<FileQueueThread>& file_queue_thread = m_threads[add_index];
                if (!file_queue_thread->IsAborting() && !file_queue_thread->IsTerminating())
                    file_queue_thread->Add(dir_entry);
            }
            std::this_thread::yield();
        }
    }

private:

    /// Name of thread.
    std::string m_name;

    // Functor used for cloning new file functors.
    std::unique_ptr<IFileFunctor> m_functor;

    std::size_t m_max_queues;
    bool m_started;
    std::vector<std::unique_ptr<FileQueueThread>> m_threads;

    /// Used when cloning functors.
    std::filesystem::directory_entry const m_empty_dir_entry;

    /// General use mutex for protecting access to multiple member variables.
    mutable std::mutex m_mutex;
};

} // namespace ocl

#endif // CPPOCL_GUARD_FILEQUEUETHREADS_HPP
