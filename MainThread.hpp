/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_MAINTHREAD_HPP
#define CPPOCL_GUARD_MAINTHREAD_HPP

#include "IThread.hpp"
#include "IFunctor.hpp"
#include <atomic>
#include <memory>

namespace ocl
{

class MainThread : public IThread
{
public:
    MainThread(IFunctor* functor, std::string const& name = "MainThread")
        : m_name(name)
        , m_id(std::this_thread::get_id())
        , m_functor(std::unique_ptr<IFunctor>(functor))
    {
    }

    MainThread(MainThread&& main_thread) noexcept
    {
        std::lock_guard<std::mutex> lock(main_thread.m_mutex);
        m_name = std::move(main_thread.m_name);
        m_id = std::this_thread::get_id();
        m_functor = std::move(main_thread.m_functor);
    }

    MainThread(MainThread const&) = delete;
    MainThread& operator=(MainThread const&) = delete;
    MainThread& operator=(MainThread&&) = delete;

    ~MainThread() override
    {
    }

// Implement IThread interface.
public:

    bool IsMainThread() const override
    {
        return true;
    }

    // Get the thread identifier.
    virtual id_type GetId() const noexcept
    {
        return m_id;
    }

    /// Always return nullptr for the main thread.
    std::thread* GetThread() noexcept override
    {
        return nullptr;
    }

    /// Always return nullptr for the main thread.
    std::thread const* GetThread() const noexcept override
    {
        return nullptr;
    }

    /// Get the name of the thread.
    std::string const& GetName() const noexcept override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_name;
    }

    /// Execute the functor.
    void Start() override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_running = true;
        if (m_functor)
            m_functor->Execute();
        m_running = false;
    }

    /// Main thread does not implement a thread loop.
    /// It's possible to derive MainThread and implement one if required.
    bool IsThreadLoop() const override
    {
        return false;
    }

    /// Return true when the thread is terminating.
    /// The current thread action or items on a thread queue will complete.
    bool IsTerminating() const override
    {
        return m_terminate;
    }

    /// The thread will terminate automatically once the functor completes.
    /// This function does not need to be called.
    void Terminate() override
    {
        m_terminate = true;
    }

    /// Return true when the thread is aborting.
    /// The current thread action will complete.
    bool IsAborting() const override
    {
        return m_abort;
    }

    void Abort() override
    {
        m_abort = true;
    }

    /// When not terminating and not aborting this will return true.
    bool IsContinuing() const override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return !m_abort && !m_terminate;
    }

    bool IsJoinable() const noexcept override final
    {
        // Never a need to join the main thread.
        return false;
    }

    void Join() override final
    {
        // Main thread never needs to join to a parent thread.
    }

    /// Tell the thread to pause.
    void Pause() override
    {
    }

    /// Tell the thread to resume, if paused.
    void Resume() override
    {
    }

    /// Return true is the thread is paused.
    bool IsPaused() const noexcept override
    {
        return false;
    }

    /// When the thread function or thread loop is running, this will be true.
    bool IsRunning() const noexcept override
    {
        return m_running;
    }

    /// When the thread has started and has not been joined, this will be true.
    bool IsActive() const noexcept override
    {
        return true;
    }

private:
    mutable std::mutex m_mutex;

    std::string m_name;
    id_type m_id; // Thread identifier.
    std::atomic_bool m_abort = false;
    std::atomic_bool m_terminate = false;
    std::atomic_bool m_running = false;
    std::unique_ptr<IFunctor> m_functor;
};

} //namespace ocl

#endif // CPPOCL_GUARD_MAINTHREAD_HPP
