/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_THREAD_HPP
#define CPPOCL_GUARD_THREAD_HPP

#include "IThread.hpp"
#include "IFunctor.hpp"
#include "utility/PlatformThreadCreate.hpp"
#include <mutex>
#include <atomic>
#include <memory>

namespace ocl
{

class Thread : public IThread
{
public:
    Thread(IFunctor* functor, std::string const& name = "Thread")
        : m_name(name)
        , m_functor(std::unique_ptr<IFunctor>(functor))
    {
        m_platform_thread = PlatformThreadCreate();
    }

    Thread(Thread&& thread) noexcept
    {
        try
        {
            std::lock_guard<std::mutex> lock(thread.m_mutex);
            m_name = std::move(thread.m_name);
            m_functor = std::move(thread.m_functor);
        }
        catch (std::exception&)
        {
        }
    }

    Thread& operator=(Thread const&) = delete;
    Thread& operator=(Thread&&) = delete;

    ~Thread()
    {
    }

// Implement IThread interface.
public:

    /// This is always a child thread.
    bool IsMainThread() const noexcept override final
    {
        return false;
    }

    // Get the thread identifier.
    virtual id_type GetId() const noexcept
    {
        return m_thread.get_id();
    }

    /// Get the thread or nullptr if the thread is not running.
    std::thread* GetThread() noexcept override
    {
        return &m_thread;
    }

    /// Get the thread or nullptr if the thread is not running for const member functions.
    std::thread const* GetThread() const noexcept override
    {
        return &m_thread;
    }

    // Get the name of the thread.
    std::string const& GetName() const noexcept override
    {
        return m_name;
    }

    /// Start the thread.
    void Start() override
    {
        m_started = true;
        m_thread = std::thread(&ThreadFunction, std::ref(*this));
        m_platform_thread->SetThreadName(m_thread, m_name.c_str());
    }

    /// Return when the threaded function is a loop.
    bool IsThreadLoop() const noexcept override
    {
        return false;
    }

    /// Return true when the thread is terminating.
    /// The current thread action or items on a thread queue will complete.
    bool IsTerminating() const override
    {
        return m_terminate;
    }

    /// The thread will terminate automatically once the functor completes.
    /// This function does not need to be called.
    void Terminate() override
    {
        m_terminate = true;
    }

    /// Return true when the thread is aborting.
    /// The current thread action will complete.
    bool IsAborting() const override
    {
        return m_abort;
    }

    /// The functor can be aborted before the thread starts.
    void Abort() override
    {
        m_abort = false;
    }

    /// When not terminating and not aborting this will return true.
    bool IsContinuing() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return !m_abort && !m_terminate;
    }

    /// Check if the thread can be joined, as it might already have been done.
    bool IsJoinable() const noexcept override
    {
        return m_thread.joinable();
    }

    /// Wait for thread to complete.
    void Join() override
    {
        if (m_started)
            m_thread.join();
        m_started = false;
    }

    /// Tell the thread to pause.
    void Pause() override
    {
    }

    /// Tell the thread to resume, if paused.
    void Resume() override
    {
    }

    /// Return true is the thread is paused.
    bool IsPaused() const noexcept override
    {
        return false;
    }

    /// When the thread function or thread loop is running, this will be true.
    bool IsRunning() const noexcept override
    {
        return m_running;
    }

    /// When the thread has started and has not been joined, this will be true.
    bool IsActive() const noexcept override
    {
        return m_started;
    }

    /// Move the thread object to this thread object.
    void Move(Thread&& thread) noexcept
    {
        std::lock_guard<std::mutex> tlock(thread.m_mutex);
        std::lock_guard<std::mutex> lock(m_mutex);
        m_name = std::move(thread.m_name);
    }

private:
    static void ThreadFunction(Thread& thread)
    {
        if (!thread.m_abort)
        {
            thread.m_running = true;
            thread.m_functor->Execute();
            thread.m_running = false;
        }
    }

private:
    mutable std::mutex m_mutex;
    std::string m_name;
    std::atomic_bool m_abort = false;
    std::atomic_bool m_terminate = false;
    std::atomic_bool m_started = false;
    std::atomic_bool m_running = false;
    std::unique_ptr<IFunctor> m_functor;

    // Ensure thread is always last, allowing access to members.
    std::thread m_thread;

    // Platform specific extensions supported through PlatformThread class.
    std::unique_ptr<PlatformThread> m_platform_thread;
};

} //namespace ocl

#endif // CPPOCL_GUARD_THREAD_HPP
