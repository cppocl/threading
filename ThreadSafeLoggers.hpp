/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_THREADSAFELOGGERS_HPP
#define CPPOCL_GUARD_THREADSAFELOGGERS_HPP

#include "ILogger.hpp"
#include "ThreadSafeConsoleLogger.hpp"
#include "ThreadSafeFileLogger.hpp"
#include <filesystem>
#include <memory>
#include <vector>
#include <string>
#include <mutex>

namespace ocl
{

class ThreadSafeLoggers
{
public:
    ThreadSafeLoggers(bool add_console_loggers = false)
    {
        if (add_console_loggers)
            AddConsoleLoggers();
    }

    /// Set a prompt string that appears before every logged info message.
    void SetInfoPrompt(std::string const& prompt)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_prompt = prompt;
    }

    /// Set a prompt string that appears before every logged warning message.
    void SetWarningPrompt(std::string const& warning_prompt)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_warning_prompt = warning_prompt;
    }

    /// Set a prompt string that appears before every logged error message.
    void SetErrorPrompt(std::string const& error_prompt)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_error_prompt = error_prompt;
    }

    /// Transfer ownership of the logger to the loggers object.
    void AddLogger(ILogger* logger, bool error_logger = false)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (error_logger)
            m_error_loggers.push_back(std::unique_ptr<ILogger>(logger));
        else
            m_loggers.push_back(std::unique_ptr<ILogger>(logger));
    }

    /// Remove all loggers.
    void Clear()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_loggers.clear();
        m_error_loggers.clear();
    }

    /// Add console loggers for stdout and stderr.
    void AddConsoleLoggers()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_loggers.push_back(std::unique_ptr<ILogger>(new ThreadSafeConsoleLogger()));
        m_error_loggers.push_back(std::unique_ptr<ILogger>(new ThreadSafeConsoleLogger(true)));
    }

    /// Add a file logger or file error logger, which will always append the next message to the file.
    void AddFileLogger(std::filesystem::path const& filename, bool error_logger = false)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (error_logger)
            m_error_loggers.push_back(std::unique_ptr<ILogger>(new ThreadSafeFileLogger(filename)));
        else
            m_loggers.push_back(std::unique_ptr<ILogger>(new ThreadSafeFileLogger(filename)));
    }

    /// Write to all loggers, excluding the error logger.
    void Write(std::string const& message)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (std::unique_ptr<ILogger>& logger : m_loggers)
            logger->Write(message);
    }

    /// Write to all loggers, excluding the error logger.
    void WriteInfo(std::string const& message)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (std::unique_ptr<ILogger>& logger : m_loggers)
            logger->Write(m_prompt + message);
    }

    void WriteWarning(std::string const& message)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (std::unique_ptr<ILogger>& logger : m_loggers)
            logger->Write(m_warning_prompt + message);
    }

    /// Write message to the console as an error.
    void WriteError(std::string const& message)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        for (std::unique_ptr<ILogger>& logger : m_error_loggers)
            logger->Write(m_error_prompt + message);
    }

private:
    std::mutex m_mutex;
    std::vector<std::unique_ptr<ILogger>> m_loggers;
    std::vector<std::unique_ptr<ILogger>> m_error_loggers;
    std::string m_prompt;
    std::string m_warning_prompt;
    std::string m_error_prompt;
};

} // namespace ocl

#endif // CPPOCL_GUARD_THREADSAFELOGGERS_HPP
