/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_THREADAPP_HPP
#define CPPOCL_GUARD_THREADAPP_HPP

#include "Threads.hpp"
#include <cstddef>
#include <atomic>
#include <memory>
#include "ThreadSafeLoggers.hpp"
#include "utility/PlatformDebug.hpp"

namespace ocl
{

class ThreadApp
{
public:
    ThreadApp(int argc, char** argv, bool always_start_thread = false, bool add_console_loggers = true)
        : m_argc(argc < 0 ? 0U : static_cast<std::size_t>(argc))
        , m_argv(argv)
        , m_terminated(false)
        , m_joined(false)
        , m_threads(always_start_thread)
        , m_loggers(std::shared_ptr<ThreadSafeLoggers>(new ThreadSafeLoggers(add_console_loggers)))
    {
    }

    virtual ~ThreadApp()
    {
        if (!m_terminated)
            Terminate();
        if (!m_joined)
            Join();
    }

    /// The Main function must be implemented by anyone wishing to use ThreadApp class.
    /// This provides a simplified interface into adding threads to the Threads object
    /// NOTE: This function must always be called from the main thread.
    virtual int Main() = 0;

    std::size_t GetArgCount() const noexcept
    {
        return m_argc;
    }

    char const* GetArg(std::size_t index) const noexcept
    {
        return index < m_argc ? m_argv[index] : nullptr;
    }

    std::size_t GetNumberOfCpuCores() const
    {
        return m_threads.GetNumberOfCpuCores();
    }

    void Add(IThread* thread)
    {
        m_threads.Add(thread);
    }

    /// Start all the threads.
    /// NOTE: The start function must always be called from the main thread.
    void Start()
    {
        m_threads.Start();
    }

    void Abort()
    {
        m_threads.Abort();
    }

    void Terminate()
    {
        m_threads.Terminate();
        m_terminated = true;
    }

    void Join()
    {
        m_threads.Join();
        m_joined = true;
    }

    /// Set a prompt string that appears before every logged info message.
    void SetInfoPrompt(std::string const& prompt)
    {
        m_loggers->SetInfoPrompt(prompt);
    }

    /// Set a prompt string that appears before every logged warning message.
    void SetWarningPrompt(std::string const& warning_prompt)
    {
        m_loggers->SetWarningPrompt(warning_prompt);
    }

    /// Set a prompt string that appears before every logged error message.
    void SetErrorPrompt(std::string const& error_prompt)
    {
        m_loggers->SetErrorPrompt(error_prompt);
    }

    /// Get the loggers, which can be passed into any custom threading classes.
    std::shared_ptr<ThreadSafeLoggers> GetLoggers() const
    {
        return m_loggers;
    }

    /// Add thread-safe console logging for stdout and stderr.
    void AddConsoleLoggers()
    {
        m_loggers->AddConsoleLoggers();
    }

    /// Add thread-safe file logging for normal messages and error messages.
    void AddFileLoggers(std::filesystem::path const& filename,
                        std::filesystem::path const& error_filename = "")
    {
        m_loggers->AddFileLogger(filename, false);
        if (!error_filename.empty())
            m_loggers->AddFileLogger(error_filename);
    }

    /// Log a message to any console and file loggers.
    void Log(std::string const& message)
    {
        m_loggers->Write(message);
    }

    /// Log a message to any console and file loggers.
    void LogInfo(std::string const& message)
    {
        m_loggers->WriteInfo(message);
    }

    /// Log a message to any console and file loggers.
    void LogWarning(std::string const& message)
    {
        m_loggers->WriteWarning(message);
    }

    /// Log a message to any console and file error loggers.
    void LogError(std::string const& message)
    {
        m_loggers->WriteError(message);
    }

    /// Get number of threads stored for the application.
    Threads::size_type GetSize() const
    {
        return m_threads.GetSize();
    }

    /// Get the thread at a given index.
    IThread* GetAt(std::size_t index)
    {
        return m_threads.GetAt(index);
    }

    /// Get the thread at a given index.
    IThread const* GetAt(std::size_t index) const
    {
        return m_threads.GetAt(index);
    }

    /// Get the tail thread.
    IThread* GetTail()
    {
        return m_threads.GetTail();
    }

    /// Get the tail thread.
    IThread const* GetTail() const
    {
        return m_threads.GetTail();
    }

    /// Return true when the build is detected as debug.
    static constexpr bool IsDebugBuild() noexcept
    {
        return PlatformDebug::IsDebugBuild();
    }

private:
    std::size_t m_argc;
    char** m_argv;
    std::atomic_bool m_terminated;
    std::atomic_bool m_joined;
    Threads m_threads;
    std::shared_ptr<ThreadSafeLoggers> m_loggers;
};

} // namespace ocl

#endif // CPPOCL_GUARD_THREADAPP_HPP
