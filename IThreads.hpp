/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_ITHREADS_HPP
#define CPPOCL_GUARD_ITHREADS_HPP

#include <string>
#include <thread>
#include <cstddef>

#include "IThread.hpp"

namespace ocl
{

/// One or more threads managed in a similar way to a thread pool.
class IThreads : public IThread
{
public:
    IThreads() = default;
    virtual ~IThreads() {}

    IThreads(IThreads const&) = delete;
    IThreads(IThreads&&) = delete;
    IThreads& operator=(IThreads const&) = delete;
    IThreads& operator=(IThreads&&) = delete;

    /// Get number of stored IThread objects.
    virtual std::size_t GetSize() const = 0;

    /// Set the number of threads.
    /// Threads to be removed will first be terminated or aborted.
    virtual void SetSize(std::size_t new_size, bool terminate = true) = 0;
};

} // namespace ocl

#endif // CPPOCL_GUARD_ITHREADS_HPP
