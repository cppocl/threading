/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_THREADSAFEFILELOGGER_HPP
#define CPPOCL_GUARD_THREADSAFEFILELOGGER_HPP

#include "ILogger.hpp"
#include <filesystem>
#include <fstream>
#include <mutex>

namespace ocl
{

class ThreadSafeFileLogger : public ILogger
{
public:
    ThreadSafeFileLogger(std::filesystem::path const& filename)
        : m_filename(filename)
    {
    }

    ~ThreadSafeFileLogger() override = default;

    ThreadSafeFileLogger(ThreadSafeFileLogger const&) = delete;
    ThreadSafeFileLogger(ThreadSafeFileLogger&&) = delete;
    ThreadSafeFileLogger& operator=(ThreadSafeFileLogger const&) = delete;
    ThreadSafeFileLogger& operator=(ThreadSafeFileLogger&&) = delete;

    void Write(std::string const& message) override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::ofstream f(m_filename, std::ios::out | std::ios::ate);
        f << message << std::endl;
    }

private:
    std::mutex m_mutex;
    std::filesystem::path m_filename;
};

} // namespace ocl

#endif // CPPOCL_GUARD_THREADSAFEFILELOGGER_HPP
