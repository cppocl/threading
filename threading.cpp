/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <iostream>
#include <chrono>
#include <thread>
#include <memory>

using std::unique_ptr;

// Include all threading classes apply "using" on the ocl namespace for the classes.
#include "threading_uses.hpp"
#include "utility/PlatformPathsCreate.hpp"

namespace
{

std::shared_ptr<ThreadSafeLocalTime> safe_local_time(new ThreadSafeLocalTime);
std::shared_ptr<ThreadSafeLoggers> loggers(new ThreadSafeLoggers(true));

/// Printing numbers and strings can optionally show the thread id for each print.
bool const show_thread_id = false;

/// Simplify using PrintThreadSafeNumber by always providing the safe print functor.
class PrintNumber : public PrintThreadSafeNumber
{
public:
    PrintNumber(long number, bool increment = false)
        : PrintThreadSafeNumber(loggers, number, increment, show_thread_id)
    {
    }
};

/// Simplify using PrintThreadSafeString by always providing the safe print functor.
class PrintString : public PrintThreadSafeString
{
public:
    PrintString(std::string const& text)
        : PrintThreadSafeString(loggers, text, show_thread_id)
    {
    }
};

class PrintTime : public PrintThreadSafeDateTime
{
public:
    PrintTime(bool show_date = false,
                    bool show_time = true,
                    bool show_human_friendly = true)

        : PrintThreadSafeDateTime(loggers,
                                  safe_local_time,
                                  show_date,
                                  show_time,
                                  show_human_friendly,
                                  show_thread_id)
    {
    }
};

/// Simplify using PrintThreadSafeDirEntry by always providing the safe print functor.
class PrintDirEntry : public PrintThreadSafeDirEntry
{
public:
    PrintDirEntry()
        : PrintThreadSafeDirEntry(loggers, show_thread_id)
    {
    }
};

class MessageFunctor : public IFunctor
{
public:
    MessageFunctor(std::string const& message)
        : m_message(message)
    {
    }

    void Execute() override
    {
        loggers->Write(m_message);
    }

private:
    std::string m_message;
};

/// Example of using ThreadApp to encapsulate your code for function main.
class MyApp : public ThreadApp
{
public:
    std::uint64_t const timer_delay_ms = 100;

    MyApp(int argc, char** argv, bool always_start_thread = false)
        : ThreadApp(argc, argv, always_start_thread)
    {
        SetInfoPrompt("INFO: ");
        SetWarningPrompt("WARNING: ");
        SetErrorPrompt("ERROR: ");
    }

    int Main() override
    {
        LogInfo("Starting...");

        // Simple of example of how to execute a functor in a separate thread in one line.
        ocl::ASync(new PrintString("ASync example text"));

        // Add three threads, including the main thread,
        // a simple thread for executing a functor and a thread queue.
        Add(new MainThread(new PrintNumber(1)));
        Add(new Thread(new PrintNumber(2), "Thread"));
        Add(new TimerThread(timer_delay_ms, new PrintTime(), "TimerThread"));

        Add(new QueueThread("QueueThread"));
        QueueThread* queue_thread = dynamic_cast<QueueThread*>(GetTail());

        Add(new QueueThreads(8, "QueueThreads"));
        QueueThreads* queue_threads = dynamic_cast<QueueThreads*>(GetTail());

        Add(new FileQueueThread(new PrintDirEntry()));
        FileQueueThread* file_queue_thread = dynamic_cast<FileQueueThread*>(GetTail());

        Add(new FileQueueThreads(new PrintDirEntry(), 8, "FileQueueThreads"));
        FileQueueThreads* file_queue_threads = dynamic_cast<FileQueueThreads*>(GetTail());

        Add(new EventThread("EventThread"));
        EventThread* event_thread = dynamic_cast<EventThread*>(GetTail());

        // Start all the threads and execute and of the existing functors.
        // NOTE: If a MainThreadLoop was added,
        //       function Start would block until the main thread loop terminated.
        Start();

        // Print a range of numbers in the queued thread.
        if (queue_thread)
        {
            for (int thread_number = 1001; thread_number <= 1100; ++thread_number)
                queue_thread->Add(new PrintNumber(thread_number));
        }

        // Print a range numbers in multiple queued threads.
        if (queue_threads)
        {
            for (int thread_number = 1101; thread_number <= 1200; ++thread_number)
                queue_threads->Add(new PrintNumber(thread_number));
        }

        std::unique_ptr<ocl::PlatformPaths> paths(ocl::PlatformPathsCreate());
        std::filesystem::path root_dir = paths->GetRootDirectory();
        std::filesystem::path prog_dir = paths->GetProgramsDirectory();

        if (file_queue_thread)
            file_queue_thread->Search(root_dir, false);

        if (file_queue_threads)
            file_queue_threads->Search(prog_dir, false);

        if (event_thread)
        {
            // Set functors that will be executed for events later when events are received.
            event_thread->Register(new ocl::NamedEvent("E1"), new MessageFunctor("Event E1"));
            event_thread->Register(new ocl::NamedEvent("E2"), new MessageFunctor("Event E2a"));
            event_thread->Register(new ocl::NamedEvent("E2"), new MessageFunctor("Event E2b"));

            // Add an event to trigger the functor to be executed.
            event_thread->Add(new ocl::NamedEvent("E2"));
        }

        // Let the timer thread do some counting and displaying time before the application ends.
        std::this_thread::sleep_for(std::chrono::milliseconds(3000));

        LogInfo("About to terminate...");

        // Any threads still active will complete whatever exists in their queues before termination.
        Terminate();

        // Wait for all the thread loops to complete and exit.
        Join();

        return 0;
    }
};

} // namespace

int main(int argc, char* argv[])
{
    MyApp my_app(argc, argv);
    return my_app.Main();
}
