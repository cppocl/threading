/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_WIN_WINPLATFORMDEBUG_HPP
#define CPPOCL_GUARD_WIN_WINPLATFORMDEBUG_HPP

namespace ocl
{

class WinPlatformDebug
{
public:
    WinPlatformDebug() = delete;
    ~WinPlatformDebug() = delete;
    WinPlatformDebug(WinPlatformDebug const&) = delete;
    WinPlatformDebug(WinPlatformDebug&&) = delete;
    WinPlatformDebug& operator=(WinPlatformDebug const&) = delete;
    WinPlatformDebug& operator=(WinPlatformDebug&&) = delete;

    /// Return true when the build is detected as debug.
    static constexpr bool IsDebugBuild() noexcept
    {
#if defined(_DEBUG)
        constexpr bool debug = true;
#else
        constexpr bool debug = false;
#endif
        return debug;
    }
};

} // namespace ocl

#endif // CPPOCL_GUARD_WIN_WINPLATFORMDEBUG_HPP
