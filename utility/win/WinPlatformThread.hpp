/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_WIN_WINPLATFORMTHREAD_HPP
#define CPPOCL_GUARD_UTILITY_WIN_WINPLATFORMTHREAD_HPP

#include <windows.h>
#include "../PlatformThread.hpp"
#include <processthreadsapi.h>
#include <cassert>

namespace ocl
{

class WinPlatformThread : public PlatformThread
{
public:
    WinPlatformThread()
        : m_thread_priority(0)
    {
    }

    WinPlatformThread(WinPlatformThread const&) = delete;
    WinPlatformThread(WinPlatformThread&&) = default;
    WinPlatformThread& operator=(WinPlatformThread const&) = delete;
    WinPlatformThread& operator=(WinPlatformThread&&) = delete;

    /// Set the thread name that will appear in the debugger when inspecting the threads.
    void SetThreadName(std::thread& thread, char const* name) override
    {
        // Solution provided by Microsoft here:
        // https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2008/xcb2z8hs(v=vs.90)?redirectedfrom=MSDN

        const DWORD MS_VC_EXCEPTION = 0x406D1388;
        DWORD thread_id = ::GetThreadId(thread.native_handle());

        THREADNAME_INFO info;
        info.dwType = 0x1000;
        info.szName = name;
        info.dwThreadID = thread_id;
        info.dwFlags = 0;

        __try
        {
            ::RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
        }
        __except (EXCEPTION_EXECUTE_HANDLER)
        {
        }
    }

    /// Get lowest available priority.
    long GetMinimumThreadPriority() const override
    {
        return -15;
    }

    /// Get highest available priority.
    long GetMaximumThreadPriority() const override
    {
        return 31;
    }

    /// Set the thread priority, which could one of the following:
    /// -15 IDLE.
    ///  -2 lowest.
    ///  -1 below normal.
    ///   0 normal.
    ///   1 above normal.
    ///   2 highest.
    ///  15 time critical.
    ///  31 Real time.
    void SetThreadPriority(std::thread& thread, long priority) override
    {
        switch (priority)
        {
        case -15:
        case -2:
        case -1:
        case 0:
        case 1:
        case 2:
        case 15:
        case 31:
            ::SetThreadPriority(thread.native_handle(), static_cast<int>(priority));
            break;
        default:
            assert(false);
        }
    }

private:

    #pragma pack(push,8)
    struct THREADNAME_INFO
    {
        DWORD dwType; // Must be 0x1000.
        LPCSTR szName; // Pointer to name (in user addr space).
        DWORD dwThreadID; // Thread ID (-1=caller thread).
        DWORD dwFlags; // Reserved for future use, must be zero.
    };
    #pragma pack(pop)

private:

    int m_thread_priority;
};

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_WIN_WINPLATFORMTHREAD_HPP
