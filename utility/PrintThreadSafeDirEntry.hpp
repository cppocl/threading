/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_PRINTTHREADDIRENTRY_HPP
#define CPPOCL_GUARD_UTILITY_PRINTTHREADDIRENTRY_HPP

#include "../IFileFunctor.hpp"
#include "ThreadSafeLoggers.hpp"
#include "ThreadDebugUtility.hpp"
#include <mutex>
#include <memory>
#include <string>
#include <filesystem>

namespace ocl
{

/// Print a directory entry using a shared thread-safe print functor.
class PrintThreadSafeDirEntry : public IFileFunctor
{
public:
    PrintThreadSafeDirEntry(std::shared_ptr<ThreadSafeLoggers> loggers,
                            bool show_thread_id = false)

        : m_loggers(loggers)
        , m_show_thread_id(show_thread_id)
    {
    }

    PrintThreadSafeDirEntry(std::filesystem::directory_entry const& dir_entry,
                            std::shared_ptr<ThreadSafeLoggers> loggers,
                            bool show_thread_id = false)

        : IFileFunctor(dir_entry)
        , m_loggers(loggers)
        , m_show_thread_id(show_thread_id)
    {
    }

    PrintThreadSafeDirEntry(PrintThreadSafeDirEntry&& ptn) = delete;
    PrintThreadSafeDirEntry& operator =(PrintThreadSafeDirEntry&& ptn) = delete;
    PrintThreadSafeDirEntry(PrintThreadSafeDirEntry const& functor) = delete;
    PrintThreadSafeDirEntry& operator =(PrintThreadSafeDirEntry const& functor) = delete;

    /// Clone this file directory object,
    /// replacing the existing directory entry with the one provided.
    IFileFunctor* Clone(std::filesystem::directory_entry const& dir_entry) override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return new PrintThreadSafeDirEntry(dir_entry, m_loggers, m_show_thread_id);
    }

    /// This function is executed within a thread.
    void Execute() override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::filesystem::directory_entry const& dir_entry = GetDirectoryEntry();

        std::string file_info = dir_entry.path().string();
        if (dir_entry.is_directory())
            file_info += " <DIR>";
        else
            file_info += " (" + std::to_string(dir_entry.file_size()) + ")";

        if (m_show_thread_id)
            file_info = ThreadDebugUtility::GetThreadIdMessage(file_info);

        m_loggers->Write(file_info);
    }

private:
    std::mutex m_mutex;
    std::shared_ptr<ThreadSafeLoggers> m_loggers;
    bool m_show_thread_id;
};

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_PRINTTHREADDIRENTRY_HPP
