/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_THREADSAFELOCALDRIVE_HPP
#define CPPOCL_GUARD_UTILITY_THREADSAFELOCALDRIVE_HPP

#include <ctime>

namespace ocl
{

/// Provide a safe way of sharing a global tm pointer.
/// NOTE: The project must use _CRT_SECURE_NO_WARNINGS define to compile for Windows platforms.
class ThreadSafeLocalTime
{
public:
    ThreadSafeLocalTime()
    {
    }

    ThreadSafeLocalTime(ThreadSafeLocalTime const&) = delete;
    ThreadSafeLocalTime(ThreadSafeLocalTime&&) = delete;
    ThreadSafeLocalTime& operator=(ThreadSafeLocalTime const&) = delete;
    ThreadSafeLocalTime& operator=(ThreadSafeLocalTime&&) = delete;

    std::tm GetLocalTime(std::time_t ttime)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::tm t = *::localtime(&ttime);
        return t;
    }

private:
    std::mutex m_mutex;
};

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_THREADSAFELOCALDRIVE_HPP
