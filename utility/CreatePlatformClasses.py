#!/usr/bin/env python

# Copyright 2022 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

copyright = [
    "/*",
    "Copyright 2022 Colin Girling",
    "",
    "Licensed under the Apache License, Version 2.0 (the \"License\");",
    "you may not use this file except in compliance with the License.",
    "You may obtain a copy of the License at",
    "",
    "http://www.apache.org/licenses/LICENSE-2.0",
    "",
    "Unless required by applicable law or agreed to in writing, software",
    "distributed under the License is distributed on an \"AS IS\" BASIS,",
    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.",
    "See the License for the specific language governing permissions and",
    "limitations under the License.",
    "*/"]

# Get the contents of a class header file.
# If create_class is True then append "Create" to the class name and
# include the original class file.
def GetClass(class_name, base_class_name = "", sub_dir = ""):
    lines = []
    lines += copyright
    lines += [""]

    if len(sub_dir) > 0:
        sub_dir = sub_dir.upper() + "_"

    define_guard = "CPPOCL_GUARD_{}{}_HPP".format(sub_dir, class_name.upper())

    lines += [
        "#ifndef {}".format(define_guard),
        "#define {}".format(define_guard),
        ""]

    if len(base_class_name) > 0:
        lines += ["#include \"../{}.hpp\"".format(base_class_name), ""]

    lines += [
        "namespace ocl",
        "{",
        ""]

    if len(base_class_name) > 0:
        lines.append("class " + class_name + " : public " + base_class_name)
    else:
        lines.append("class " + class_name)

    virtual  = "" if len(base_class_name) > 0 else "virtual "
    override = " override" if len(base_class_name) > 0 else ""

    lines += [
        "{",
        "public:",
        "    {}() = default;".format(class_name),
        "",
        "    {}~{}(){} = default;".format(virtual, class_name, override),
        "",
        "    {}({} const&) = delete;".format(class_name, class_name),
        "    {}({}&&) = delete;".format(class_name, class_name),
        "    {}& operator=({} const&) = delete;".format(class_name, class_name),
        "    {}& operator=({}&&) = delete;".format(class_name, class_name),
        "};",
        "",
        "} // namespace ocl",
        "",
        "#endif // {}".format(define_guard)]

    return lines

def WriteLines(filename, lines):
    print("Creating {}".format(filename))
    f = open(filename, "w")

    line_no = 0
    while line_no < len(lines):
        lines[line_no] = lines[line_no] + "\n"
        line_no += 1

    f.writelines(lines);
    f.close()

def CreateClassHeaderFile(class_name, sub_dir = "", create_class = False):

    base_class_name = ""

    if len(sub_dir) > 0:
        base_class_name = class_name
        platform = sub_dir
        platform = platform[0].upper() + platform[1:]
        class_name = platform + class_name

    file_content = \
        ""

    filename = os.path.join(sub_dir, class_name + ".hpp")
    lines = GetClass(class_name, base_class_name, sub_dir)
    WriteLines(filename, lines)

def CreatePlatformClassHeaderFile(class_name):
    define_guard = "CPPOCL_GUARD_{}CREATE_HPP".format(class_name.upper())

    lines = []
    lines += copyright
    lines += [""]
    lines += [
        "#ifndef {}".format(define_guard),
        "#define {}".format(define_guard),
        ""]

    lines += [
        "#include <memory>",
        "",
        "#if defined(_WIN32) || defined(_WIN64) || defined(_MSC_VER)",
        "#include \"win/Win{}.hpp\"".format(class_name),
        "#else",
        "#include \"linux/Linux{}.hpp\"".format(class_name),
        "#endif",
        "",

        "namespace ocl",
        "{",
        "",
        "std::unique_ptr<{}> {}Create()".format(class_name, class_name),
        "{",
        "#if defined(_WIN32) || defined(_WIN64) || defined(_MSC_VER)",
        "    return std::unique_ptr<{}>(new Win{}());".format(class_name, class_name),
        "#else",
        "    return std::unique_ptr<{}>(new Linux{}());".format(class_name, class_name),
        "#endif",
        "}",
        "",
        "} // namespace ocl",
        ""]

    lines += ["#endif // {}".format(define_guard)]

    filename = class_name + "Create.hpp"
    WriteLines(filename, lines)

def CreateClasses(class_name):
    CreateClassHeaderFile(class_name)
    CreateClassHeaderFile(class_name, "linux")
    CreateClassHeaderFile(class_name, "win")
    CreatePlatformClassHeaderFile(class_name)

if __name__ == "__main__":

    print("starting...")

    if len(sys.argv) > 1:
        class_name = sys.argv[1]
        CreateClasses(class_name)
