/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_PRINTTHREADNUMBER_HPP
#define CPPOCL_GUARD_UTILITY_PRINTTHREADNUMBER_HPP

#include "../IFunctor.hpp"
#include "ThreadSafeLoggers.hpp"
#include "ThreadDebugUtility.hpp"
#include <mutex>
#include <memory>
#include <string>

namespace ocl
{

/// Print a number using a shared thread-safe print functor.
class PrintThreadSafeNumber : public IFunctor
{
public:
    PrintThreadSafeNumber(std::shared_ptr<ThreadSafeLoggers> loggers,
                          long number,
                          bool increment = false,
                          bool show_thread_id = false)

        : m_loggers(loggers)
        , m_number(number)
        , m_increment(increment)
        , m_show_thread_id(show_thread_id)
    {
    }

    PrintThreadSafeNumber(PrintThreadSafeNumber&& ptn) = delete;
    PrintThreadSafeNumber& operator =(PrintThreadSafeNumber&& ptn) = delete;
    PrintThreadSafeNumber(PrintThreadSafeNumber const& functor) = delete;
    PrintThreadSafeNumber& operator =(PrintThreadSafeNumber const& functor) = delete;

    /// Change the number for next output to terminal.
    void SetNumber(int number)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_number = number;
    }

    /// This function is executed within a thread.
    void Execute() override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::string message;

        if (m_show_thread_id)
            message = ThreadDebugUtility::GetThreadIdMessage(m_number);
        else
            message = std::to_string(m_number);

        m_loggers->Write(message);

        if (m_increment)
            ++m_number;
    }

private:
    std::mutex m_mutex;
    std::shared_ptr<ThreadSafeLoggers> m_loggers;
    long m_number;
    bool m_increment;
    bool m_show_thread_id;
};

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_PRINTTHREADNUMBER_HPP
