/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_THREADDEBUGUTILITY_HPP
#define CPPOCL_GUARD_UTILITY_THREADDEBUGUTILITY_HPP

#include <thread>
#include <sstream>
#include <string>

namespace ocl
{

class ThreadDebugUtility
{
public:
    static std::string GetThreadIdString()
    {
        std::ostringstream ostr;
        ostr << std::this_thread::get_id();
        return ostr.str();
    }

    static std::string GetThreadIdMessage(std::string const& value)
    {
        return GetThreadIdString() + ": " + value;
    }

    static std::string GetThreadIdMessage(int value)
    {
        return GetThreadIdString() + ": " + std::to_string(value);
    }

    static std::string GetThreadIdMessage(unsigned int value)
    {
        return GetThreadIdString() + ": " + std::to_string(value);
    }

    static std::string GetThreadIdMessage(long value)
    {
        return GetThreadIdString() + ": " + std::to_string(value);
    }

    static std::string GetThreadIdMessage(unsigned long value)
    {
        return GetThreadIdString() + ": " + std::to_string(value);
    }
};

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_THREADDEBUGUTILITY_HPP
