/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_PLATFORMTHREADCREATE_HPP
#define CPPOCL_GUARD_PLATFORMTHREADCREATE_HPP

#include <memory>

#if defined(_WIN32) || defined(_WIN64) || defined(_MSC_VER)
#include "win/WinPlatformThread.hpp"
#else
#include "linux/LinuxPlatformThread.hpp"
#endif

namespace ocl
{

std::unique_ptr<PlatformThread> PlatformThreadCreate()
{
#if defined(_WIN32) || defined(_WIN64) || defined(_MSC_VER)
    return std::unique_ptr<PlatformThread>(new WinPlatformThread());
#else
    return std::unique_ptr<PlatformThread>(new LinuxPlatformThread());
#endif
}

} // namespace ocl

#endif // CPPOCL_GUARD_PLATFORMTHREADCREATE_HPP

