/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_LINUX_LINUXPLATFORMPATHS_HPP
#define CPPOCL_GUARD_LINUX_LINUXPLATFORMPATHS_HPP

#include "../PlatformPaths.hpp"

namespace ocl
{

class LinuxPlatformPaths : public PlatformPaths
{
public:
    LinuxPlatformPaths() = default;

    ~LinuxPlatformPaths() override = default;

    LinuxPlatformPaths(LinuxPlatformPaths const&) = delete;
    LinuxPlatformPaths(LinuxPlatformPaths&&) = delete;
    LinuxPlatformPaths& operator=(LinuxPlatformPaths const&) = delete;
    LinuxPlatformPaths& operator=(LinuxPlatformPaths&&) = delete;

    /// Get the root directory.
    std::filesystem::path GetRootDirectory() const override { return "/"; }

    /// Get the path for the programs directory.
    std::filesystem::path GetProgramsDirectory() const override { return "/usr/bin"; }
};

} // namespace ocl

#endif // CPPOCL_GUARD_LINUX_LINUXPLATFORMPATHS_HPP
