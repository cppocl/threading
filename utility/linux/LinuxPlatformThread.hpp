/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_WIN_LINUXPLATFORMTHREAD_HPP
#define CPPOCL_GUARD_UTILITY_WIN_LINUXPLATFORMTHREAD_HPP

#include "../PlatformThread.hpp"

namespace ocl
{

class LinuxPlatformThread : public PlatformThread
{
public:
    LinuxPlatformThread() = default;

    LinuxPlatformThread(LinuxPlatformThread const&) = delete;
    LinuxPlatformThread(LinuxPlatformThread&&) = default;
    LinuxPlatformThread& operator=(LinuxPlatformThread const&) = delete;
    LinuxPlatformThread& operator=(LinuxPlatformThread&&) = delete;

    /// Set the thread name that will appear in the debugger when inspecting the threads.
    void SetThreadName(std::thread& /*thread*/, char const* /*name*/) override
    {
    }

    /// Get lowest available priority.
    long GetMinimumThreadPriority() const override
    {
        return 0;
    }

    /// Get highest available priority.
    long GetMaximumThreadPriority() const override
    {
        return 0;
    }

    /// Set the thread priority, which could one of the following:
    /// -15 IDLE.
    ///  -2 lowest.
    ///  -1 below normal.
    ///   0 normal.
    ///   1 above normal.
    ///   2 highest.
    ///  15 time critical.
    ///  31 Real time.
    void SetThreadPriority(std::thread& /*thread*/, long /*priority*/) override
    {
    }

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_WIN_LINUXPLATFORMTHREAD_HPP
