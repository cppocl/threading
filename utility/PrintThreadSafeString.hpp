/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_PRINTTHREADSTRING_HPP
#define CPPOCL_GUARD_UTILITY_PRINTTHREADSTRING_HPP

#include "../IFunctor.hpp"
#include "ThreadSafeLoggers.hpp"
#include "ThreadDebugUtility.hpp"
#include <memory>
#include <string>

namespace ocl
{

/// Print a string using a shared thread-safe print functor.
class PrintThreadSafeString : public IFunctor
{
public:
    PrintThreadSafeString(std::shared_ptr<ThreadSafeLoggers> loggers,
                          std::string const& text,
                          bool show_thread_id = false)

        : m_loggers(loggers)
        , m_text(text)
        , m_show_thread_id(show_thread_id)
    {
    }

    PrintThreadSafeString(PrintThreadSafeString&& ptn) = delete;
    PrintThreadSafeString& operator =(PrintThreadSafeString&& ptn) = delete;
    PrintThreadSafeString(PrintThreadSafeString const& functor) = delete;
    PrintThreadSafeString& operator =(PrintThreadSafeString const& functor) = delete;

    /// This function is executed within a thread.
    void Execute() override
    {
        std::string message;

        if (m_show_thread_id)
            message = ThreadDebugUtility::GetThreadIdMessage(m_text);
        else
            message = m_text;

        m_loggers->Write(message);
    }

private:
    std::shared_ptr<ThreadSafeLoggers> m_loggers;
    std::string m_text;
    bool m_show_thread_id;
};

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_PRINTTHREADSTRING_HPP
