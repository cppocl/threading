/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_PLATFORMTHREAD_HPP
#define CPPOCL_GUARD_PLATFORMTHREAD_HPP

#include <thread>

namespace ocl
{

/// Platform independent class with functions for manipulating a
/// std::thread object on multiple platforms.
/// The standard library does not support naming threads or setting thread priority.
class PlatformThread
{
public:
    PlatformThread() = default;
    virtual ~PlatformThread() = default;

    PlatformThread(PlatformThread const&) = delete;
    PlatformThread(PlatformThread&&) = default;
    PlatformThread& operator=(PlatformThread const&) = delete;
    PlatformThread& operator=(PlatformThread&&) = delete;

    /// Set the thread name that will appear in the debugger when inspecting the threads.
    virtual void SetThreadName(std::thread& thread, char const* name) = 0;

    /// Get lowest available priority.
    virtual long GetMinimumThreadPriority() const = 0;

    /// Get highest available priority.
    virtual long GetMaximumThreadPriority() const = 0;

    /// Set the thread priority, which could be negative or positive, depending on platform.
    virtual void SetThreadPriority(std::thread& thread, long priority) = 0;
};

} // namespace ocl

#endif // CPPOCL_GUARD_PLATFORMTHREAD_HPP
