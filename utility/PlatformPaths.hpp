/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_PLATFORMPATHS_HPP
#define CPPOCL_GUARD_PLATFORMPATHS_HPP

#include <filesystem>

namespace ocl
{

class PlatformPaths
{
public:
    PlatformPaths() = default;

    virtual ~PlatformPaths() = default;

    PlatformPaths(PlatformPaths const&) = delete;
    PlatformPaths(PlatformPaths&&) = delete;
    PlatformPaths& operator=(PlatformPaths const&) = delete;
    PlatformPaths& operator=(PlatformPaths&&) = delete;

    /// Get the root directory for the primary drive, e.g. "C:\\" or "/"
    virtual std::filesystem::path GetRootDirectory() const = 0;

    /// Get the path for the programs directory, e.g. "C:\Program Files" or "/usr/bin"
    virtual std::filesystem::path GetProgramsDirectory() const = 0;
};

} // namespace ocl

#endif // CPPOCL_GUARD_PLATFORMPATHS_HPP
