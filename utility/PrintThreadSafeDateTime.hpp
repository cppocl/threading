/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_UTILITY_PRINTTHREADDATETIME_HPP
#define CPPOCL_GUARD_UTILITY_PRINTTHREADDATETIME_HPP

#include "../IFunctor.hpp"
#include "ThreadSafeLoggers.hpp"
#include "ThreadDebugUtility.hpp"
#include "ThreadSafeLocalTime.hpp"
#include <ctime>
#include <chrono>
#include <memory>
#include <string>
#include <thread>

namespace ocl
{

    /// Print a directory entry using a shared thread-safe print functor.
    class PrintThreadSafeDateTime : public IFunctor
    {
    public:
        PrintThreadSafeDateTime(std::shared_ptr<ThreadSafeLoggers> loggers,
                                std::shared_ptr<ocl::ThreadSafeLocalTime> safe_local_time,
                                bool show_date = true,
                                bool show_time = true,
                                bool show_human_friendly = true,
                                bool show_thread_id = false)

            : m_loggers(loggers)
            , m_safe_local_time(safe_local_time)
            , m_show_thread_id(show_thread_id)
            , m_date_friendly_fmt("%d %b %Y")
            , m_date_numeric_fmt("%d/%m/%Y")
            , m_date_fmt(show_human_friendly ? m_date_friendly_fmt : m_date_numeric_fmt)
            , m_time_fmt("%H:%M:%S")
            , m_date_time_fmt(m_date_fmt + " " + m_time_fmt)
            , m_fmt(show_date ? (show_time ? m_date_time_fmt : m_date_fmt) : m_time_fmt)
        {
        }

        PrintThreadSafeDateTime(PrintThreadSafeDateTime&& ptn) = delete;
        PrintThreadSafeDateTime& operator =(PrintThreadSafeDateTime&& ptn) = delete;
        PrintThreadSafeDateTime(PrintThreadSafeDateTime const& functor) = delete;
        PrintThreadSafeDateTime& operator =(PrintThreadSafeDateTime const& functor) = delete;

        /// This function is executed within a thread.
        void Execute() override
        {
            auto now = std::chrono::system_clock::now();
            std::time_t ttime = std::chrono::system_clock::to_time_t(now);
            std::string date_time;
            char szDateTime[100];
            std::tm tmbuf {};

            std::tm tm = m_safe_local_time->GetLocalTime(ttime);
            std::strftime(szDateTime, sizeof(szDateTime), m_fmt.c_str(), &tm);
            date_time = szDateTime;
            m_loggers->Write(date_time);
        }

    private:
        std::shared_ptr<ThreadSafeLoggers> m_loggers;
        std::shared_ptr<ocl::ThreadSafeLocalTime> m_safe_local_time;
        bool m_show_thread_id;

        // Various date and time formats supported by the functor.
        std::string const m_date_friendly_fmt;
        std::string const m_date_numeric_fmt;
        std::string const m_date_fmt;
        std::string const m_time_fmt;
        std::string const m_date_time_fmt;

        // The date/time format to be used for each Execute.
        std::string const m_fmt;
    };

} // namespace ocl

#endif // CPPOCL_GUARD_UTILITY_PRINTTHREADDATETIME_HPP
