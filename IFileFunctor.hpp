/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_IFILEFUNCTOR_HPP
#define CPPOCL_GUARD_IFILEFUNCTOR_HPP

#include <filesystem>

namespace ocl
{

// Define an interface that can be used to execute a function or member function of a class.
class IFileFunctor
{
public:
    IFileFunctor() = default;

    IFileFunctor(std::filesystem::directory_entry const& dir_entry)
        : m_dir_entry(dir_entry)
    {
    }

    IFileFunctor(IFileFunctor const&) = delete;
    IFileFunctor(IFileFunctor&&) = delete;
    IFileFunctor& operator=(IFileFunctor const&) = delete;
    IFileFunctor& operator=(IFileFunctor&&) = delete;

    virtual ~IFileFunctor() {}

    /// Implement a cloning function that can create the IFileFunctor
    /// that can be used within a search function and sets the directory entry.
    virtual IFileFunctor* Clone(std::filesystem::directory_entry const& dir_entry) = 0;

    /// The function that will be executed within a thread.
    virtual void Execute() = 0;

    // Implement the functor operator to call Execute function.
    void operator()()
    {
        Execute();
    }

    /// Get the directory entry for derived classes or clients to access.
    std::filesystem::directory_entry const& GetDirectoryEntry() const noexcept
    {
        return m_dir_entry;
    }

    /// Set the directory entry for derived classes and clients to access.
    void SetDirectoryEntry(std::filesystem::directory_entry const& dir_entry)
    {
        m_dir_entry = dir_entry;
    }

    /// Set the directory entry for derived classes and clients to access,
    /// by moving the directory entry provided.
    void SetDirectoryEntry(std::filesystem::directory_entry&& dir_entry) noexcept
    {
        m_dir_entry = std::move(dir_entry);
    }

private:
    std::filesystem::directory_entry m_dir_entry;
};

} // namespace ocl

#endif // CPPOCL_GUARD_IFILEFUNCTOR_HPP
