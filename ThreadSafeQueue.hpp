/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_THREADSAFEQUEUE_HPP
#define CPPOCL_GUARD_THREADSAFEQUEUE_HPP

#include <mutex>
#include <queue>
#include <cstddef>
#include <exception>

namespace ocl
{

template<typename Type>
class ThreadSafeQueue
{
public:
    ThreadSafeQueue()
    {
    }

    virtual ~ThreadSafeQueue() = default;

    /// Guarantee the constructor can never throw while moving thread safe queues.
    ThreadSafeQueue(ThreadSafeQueue&& queue) noexcept
    {
        Move(std::move(queue));
    }

    ThreadSafeQueue& operator=(ThreadSafeQueue&& queue) noexcept
    {
        Move(std::move(queue));
        return *this;
    }

    ThreadSafeQueue(ThreadSafeQueue const&) = delete;
    ThreadSafeQueue& operator=(ThreadSafeQueue const&) = delete;

    /// Push copy of value onto queue in a thread-safe way.
    void Push(Type const& value)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_values.push(value);
    }

    /// Move value onto the queue in a thread-safe way.
    void Push(Type&& value)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_values.push(std::move(value));
    }

    /// Pop value off the queue in a thread-safe way.
    bool Pop(Type& popped_value)
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        bool popped = !m_values.empty();
        if (popped)
        {
            popped_value = std::move(m_values.front());
            m_values.pop();
        }
        return popped;
    }

    /// Return true when the queue is empty.
    bool IsEmpty() const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_values.empty();
    }

    /// Empty the queue.
    void Clear()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        std::queue<Type> empty;
        std::swap(m_values, empty);
    }

    /// Get the size of the queue.
    std::size_t GetSize() const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_values.size();
    }

    /// Move the queue to this object.
    void Move(ThreadSafeQueue&& queue) noexcept
    {
        try
        {
            queue.m_mutex.lock();
            std::lock_guard<std::mutex> lock(m_mutex);
            m_values = std::move(queue.m_values);
            queue.m_mutex.unlock();
        }
        catch (std::exception&)
        {
            try
            {
                Clear();
            }
            catch (std::exception&)
            {
            }
        }
    }

private:
    mutable std::mutex m_mutex;
    std::queue<Type> m_values;
};

} // namespace ocl

#endif // CPPOCL_GUARD_THREADSAFEQUEUE_HPP
