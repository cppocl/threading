/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_THREADS_HPP
#define CPPOCL_GUARD_THREADS_HPP

#include "IThread.hpp"
#include <deque>
#include <mutex>
#include <string>
#include <memory>
#include <thread>

namespace ocl
{

/// List of threads, which is not thread-safe and needs to be handled externally.
class Threads
{
public:
    typedef std::deque<IThread>::size_type size_type;

public:
    Threads(bool always_start_thread = false)
        : m_always_start_thread(always_start_thread)
    {
    }

    ~Threads()
    {
    }

    std::size_t GetNumberOfCpuCores() const
    {
        return std::thread::hardware_concurrency();
    }

    /// Add the thread to the list of threads.
    void Add(IThread* thread)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_threads.push_back(std::unique_ptr<IThread>(thread));
        if (m_always_start_thread)
            m_threads.back()->Start();
    }

    /// Remove the last item of the queue and return it.
    std::unique_ptr<IThread> Pop()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        std::unique_ptr<IThread> back = std::move(m_threads.back());
        m_threads.pop_back();
        return back;
    }

    /// Start all the stored threads.
    /// NOTE: This must be called from the main thread.
    void Start()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_always_start_thread = true;
        std::unique_ptr<IThread>* pMainThread = nullptr;
        for (auto& thread : m_threads)
        {
            if (thread->IsMainThread())
                pMainThread = &thread;
            else
                thread->Start();
        }

        // Always start the main thread last.
        // This gives other threads the opportunity to run before the main thread blocks.
        if (pMainThread)
            (*pMainThread)->Start();
    }

    /// Terminate all threads, completing any remaining activities.
    void Terminate()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (auto& thread : m_threads)
            thread->Terminate();
    }

    /// Abort all threads, ignoring any remaining activities.
    void Abort()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (auto& thread : m_threads)
            thread->Abort();
        m_threads.clear();
    }

    /// Join the threads to the main thread.
    void Join()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (auto& thread : m_threads)
            thread->Join();
    }

    /// Get number of stores threads.
    size_type GetSize() const
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return m_threads.size();
    }

    /// Find thread by the name, or return nullptr when not found.
    IThread* Find(std::string const& name)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<IThread>& thread : m_threads)
            if (thread->GetName() == name)
                return thread.get();
        return nullptr;
    }

    /// Find thread by the name, or return nullptr when not found.
    IThread const* Find(std::string const& name) const
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<IThread> const& thread : m_threads)
            if (thread->GetName() == name)
                return thread.get();
        return nullptr;
    }

    /// Get the thread at a specified index.
    IThread* GetAt(std::size_t index)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return index < m_threads.size() ? m_threads[index].get() : nullptr;
    }

    /// Get the thread at a specified index.
    IThread const* GetAt(std::size_t index) const
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return index < m_threads.size() ? m_threads[index].get() : nullptr;
    }

    /// Get last thread or return null when there the list is empty.
    IThread* GetTail()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return m_threads.empty() ? nullptr : m_threads.back().get();
    }

    /// Get last thread or return null when there the list is empty.
    IThread const* GetTail() const
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return m_threads.empty() ? nullptr : m_threads.back().get();
    }

private:
    std::deque<std::unique_ptr<IThread>> m_threads;
    bool m_always_start_thread = false;
    mutable std::mutex m_mutex;
};

} // namespace ocl

#endif // CPPOCL_GUARD_THREADS_HPP
