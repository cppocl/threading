# Threading

C++ threading library, designed to execute functors.

## Classes

- `IFunctor`

    Interface for any user defined functors.

    - `virtual void Execute() = 0`

        Implement the `Execute` function that will run within the thread.

- `IFileFunctor`

    Interface for any user defined functors.

    - `virtual void Execute() = 0`

        Implement the `Execute` function that will run within the thread.

    - `virtual IFileFunctor* Clone(std::filesystem::directory_entry const& dir_entry) = 0`

        Clone an existing file functor with a new directory entry.

- `IEvent`

    Interface for any user defined events.

    - `virtual IEvent* Clone() = 0`

        Implement the `Clone` function to create a copy of the existing event.

    - `virtual bool IsLess(IEvent*) const = 0`

        Implement the `IsLess` function to compare with another event.

- `IThread`

    Interface for all provided thread classes.

    Users can extend the library with their own threading classes derived from `IThread`.

    - `virtual void Start() = 0`

        Start the thread and execute the functor(s).

    - `virtual void Terminate() = 0`

        Terminate the thread, allowing any existing functors to complete.

    - `virtual void Abort() = 0`

        Abort the thread, only completing the currently executing functor.

- `IThreads`

    Interface for containing one or more threads.

    - `virtual std::size_t GetSize() const = 0`

        Get the number of stored threads.

    - `virtual void AddThreads(std::size_t count) = 0`

        Add the specified number of threads.

    - `virtual void RemoveThreads(std::size_t count, bool terminate) = 0`

        Remove the specified number of threads, either terminating or aborting each thread.

- `MainThread`

    The main thread, which can be used as the entry point for function `main`.

- `Thread`

    A basic thread that will execute the functor on a separate thread.

- `QueueThread`

    A queue containing functors, executed in the sequence they were added on a single thread.

- `QueueThreads`

    A queue or multiple queues, each queue existing on their own thread.

- `FileQueueThread`

    A queue containing file functors, which is populated with the call to the `Search` function.

    - `FileQueueThread(IFileFunctor* functor, std::string const& name = "FileThread")`

        Constructor which provides a file functor used to replication when calling the `Search` function.

    - `void Search(std::filesystem::path const& search_path, bool recursive)`

        Search within the provided path and execute the cloned functor for each matching file.

- `FileQueueThreads`

    A queue or multiple file queues, each file queue existing on their own thread.

- `TimerThread`

    A thread running as a timer, which sleeps on the interval specified.

    The timer is adjusted based on the execution time of the functor, keeping the interval consistent.

    If the functor takes longer to execute than the specified delay, there will be no delay between functor calls.

- `EventThread`

    A thread that allows the user the register an event to a functor.

    The thread will continue to run until the `Abort` or `Terminate` function is called, or an abort or terminate event is added to the event thread.

- `Threads`

    Container of threads derived from `IThread` interface.

     - `void Add(IThread* thread)`

        Add a thread to the container of threads.

    - `std::unique_ptr<IThread> Pop()`

        Remove the last added thread from the threads.

    - `void Start()`

        Start all the stored threads.

        Note than any subsequent threads added will automatically start after this call.

- `ThreadSafePrint`

    A utility helper class with a thread-safe wrapper for std::cout used to print text lines to terminal or console.

- `ThreadApp`

    A base class for a that is used for multi-threaded applications, which keeps function `main` cleaner.
    The user only needs to override `Main()` function and create a `ThreadApp` object.

    - `ThreadApp(int argc, char** argv, bool always_start_thread)`

        Constructor which stores `argc` and `argv` from function `main`.

    - `std::size_t GetArgCount() const noexcept`

        Get the number of arguments passed into function `main`.

    - `char const* GetArg(std::size_t index) const noexcept`

        Get the argument string at a speicified index passed into function `main`.

    - `void Add(IThread* thread)`

        Add any thread derived from `IThread` interface.

    - `Start()`

        Start all the threads stored after having been added.

    - `void Terminate()`

        Terminate all the running threads.

    - `void Abort()`

        Abort all the running threads.

    - `void Join()`

        Wait for all the threads to join the main thread.

    - `IThread* GetAt(std::size_t index)`
    - `IThread const* GetAt(std::size_t index) const`

        Get a thread at a specified index.

    - `IThread* GetTail()`
    - `IThread const* GetTail() const`

        Get the last added thread stored.

## Examples

```

#include <iostream>
#include <chrono>
#include <thread>
#include <memory>

using std::unique_ptr;

// Include all threading classes apply "using" on the ocl namespace for the classes.
#include "threading_uses.hpp"

namespace
{
    /// The thread safe print functor can be shared between multiple print functors.
    std::shared_ptr<ThreadSafePrint> safe_print(new ThreadSafePrint);

    std::shared_ptr< ThreadSafeLocalTime> safe_local_time(new ThreadSafeLocalTime);

    /// Printing numbers and strings can optionally show the thread id for each print.
    bool const show_thread_id = false;

    /// Simplify using PrintThreadSafeNumber by always providing the safe print functor.
    class PrintNumber : public PrintThreadSafeNumber
    {
    public:
        PrintNumber(long number, bool increment = false)
            : PrintThreadSafeNumber(safe_print, number, increment, show_thread_id)
        {
        }
    };

    /// Simplify using PrintThreadSafeString by always providing the safe print functor.
    class PrintString : public PrintThreadSafeString
    {
    public:
        PrintString(std::string const& text)
            : PrintThreadSafeString(safe_print, text, show_thread_id)
        {
        }
    };

    class PrintTime : public PrintThreadSafeDateTime
    {
    public:
        PrintTime(bool show_date = false,
                      bool show_time = true,
                      bool show_human_friendly = true)

            : PrintThreadSafeDateTime(safe_print,
                                      safe_local_time,
                                      show_date,
                                      show_time,
                                      show_human_friendly,
                                      show_thread_id)
        {
        }
    };

    /// Simplify using PrintThreadSafeDirEntry by always providing the safe print functor.
    class PrintDirEntry : public PrintThreadSafeDirEntry
    {
    public:
        PrintDirEntry()
            : PrintThreadSafeDirEntry(safe_print, show_thread_id)
        {
        }
    };

    /// Example of using ThreadApp to encapsulate your code for function main.
    class MyApp : public ThreadApp
    {
    public:
        std::uint64_t const timer_delay_ms = 100;

        MyApp(int argc, char** argv, bool always_start_thread = false)
            : ThreadApp(argc, argv, always_start_thread)
        {
        }

        int Main() override
        {
            // Simple of example of how to execute a functor in a separate thread in one line.
            ocl::ASync(new PrintString("ASync example text"));

            // Add three threads, including the main thread,
            // a simple thread for executing a functor and a thread queue.
            Add(new MainThread(new PrintNumber(1)));
            Add(new Thread(new PrintNumber(2), "Thread"));
            Add(new TimerThread(timer_delay_ms, new PrintTime(), "TimerThread"));
            Add(new QueueThread("QueueThread"));

            QueueThread* queue_thread = dynamic_cast<QueueThread*>(GetTail());

            Add(new QueueThreads(8, "QueueThreads"));

            QueueThreads* queue_threads = dynamic_cast<QueueThreads*>(GetTail());

            Add(new FileQueueThread(new PrintDirEntry()));

            FileQueueThread* file_queue_thread = dynamic_cast<FileQueueThread*>(GetTail());

            Add(new FileQueueThreads(new PrintDirEntry(), 8, "FileQueueThreads"));

            FileQueueThreads* file_queue_threads = dynamic_cast<FileQueueThreads*>(GetTail());

            // Start all the threads and execute and of the existing functors.
            Start();

            // Print a range of numbers in the queued thread.
            if (queue_thread)
            {
                for (int thread_number = 1001; thread_number <= 1100; ++thread_number)
                    queue_thread->Add(new PrintNumber(thread_number));
            }

            // Print a range numbers in multiple queued threads.
            if (queue_threads)
            {
                for (int thread_number = 1101; thread_number <= 1200; ++thread_number)
                    queue_threads->AddFunctor(new PrintNumber(thread_number));
            }

#if defined(_WIN32) || defined(_MSC_VER)
            std::string search_path1 = "C:\\";
            std::string search_path2 = "C:\\Program Files (x86)";
#else
            std::string search_path1 = "/";
            std::string search_path2 = "/usr";
#endif

            if (file_queue_thread)
                file_queue_thread->Search(search_path1, false);

            if (file_queue_threads)
                file_queue_threads->Search(search_path2, false);

            // Let the timer thread do some counting and displaying time before the application ends.
            std::this_thread::sleep_for(std::chrono::milliseconds(3000));

            // Any threads still active will complete whatever exists in their queues before termination.
            Terminate();

            // Wait for all the thread loops to complete and exit.
            Join();

            return 0;
        }
    };

} // namespace

int main(int argc, char* argv[])
{
    MyApp my_app(argc, argv);
    return my_app.Main();
}

```

## Design

UML Diagram of threading library.

![UML diagram](ThreadingUML.png)

## License

Apache 2.0
