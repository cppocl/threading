/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_ASYNC_HPP
#define CPPOCL_GUARD_ASYNC_HPP

#include "IFunctor.hpp"
#include "Thread.hpp"
#include <memory>

namespace ocl
{

class ASyncThread
{
public:
    ASyncThread(IFunctor* functor, std::string const& name = "ASyncThread")
        : m_thread(new ocl::Thread(functor, name))
    {
        m_thread->Start();
        m_thread->Terminate();
        m_thread->Join();
        m_thread.reset();
    }

private:
    std::unique_ptr<ocl::Thread> m_thread;
};

void ASync(IFunctor* functor, std::string const& name = "ASync")
{
    std::unique_ptr<ASyncThread> async(new ASyncThread(functor, name));
}

} // namespace ocl

#endif // CPPOCL_GUARD_ASYNC_HPP
