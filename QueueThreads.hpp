/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_QUEUETHREADS_HPP
#define CPPOCL_GUARD_QUEUETHREADS_HPP

#include "IThreads.hpp"
#include "IFunctor.hpp"
#include "QueueThread.hpp"
#include <mutex>
#include <atomic>
#include <exception>
#include <condition_variable>
#include <memory>
#include <string>

namespace ocl
{

// Thread class containing two threads to push and pop from a thread-safe queue of IFunctor objects.
//
// Guarantees that IFunctor objects can be pushed onto a queue and immediately returned,
// without waiting for the queue to process existing items.
class QueueThreads final : public IThreads
{
public:

    /// Construct a named QueueThreads object.
    QueueThreads(std::size_t max_queues, std::string const& name = "QueueThreads")
        : m_name(name)
        , m_max_queues(max_queues)
        , m_started(false)
    {
        QueueThreads::AddThreads(max_queues);
    }

    /// Move the QueueThreads object.
    /// There is the potential for std::queue constructor to throw an exception,
    /// is it does not specify noexcept.
    QueueThreads(QueueThreads&& queue_thread) noexcept
    {
        try
        {
            queue_thread.Pause();
            queue_thread.m_mutex.lock();
            m_name = std::move(queue_thread.m_name);
            m_max_queues = queue_thread.m_max_queues;
            queue_thread.m_mutex.unlock();
        }
        catch (std::exception&)
        {
        }
    }

    QueueThreads(QueueThreads const&) = delete;
    QueueThreads& operator=(QueueThreads const&) = delete;
    QueueThreads& operator=(QueueThreads&&) = delete;

    ~QueueThreads()
    {
    }

/// Implement IThreads interface.
public:

    /// Get number of stored IThread objects.
    std::size_t GetSize() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        return m_threads.size();
    }

    /// Set the number of threads.
    /// Threads to be removed will first be terminated or aborted.
    void SetSize(std::size_t new_size, bool terminate = true) override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        std::size_t size = m_threads.size();
        if (new_size > size)
            AddThreads(new_size - size);
        else if (new_size < size)
            RemoveThreads(size - new_size, terminate);
    }

    /// Add the functor to the next available queue with the least amount of work to do.
    void Add(IFunctor* functor)
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        std::size_t add_index = GetAddIndex();
        auto& queue_thread = m_threads[add_index];
        queue_thread->Add(functor);
    }

/// Implement IThread interface.
public:

    /// This is always a child thread.
    bool IsMainThread() const noexcept override
    {
        return false;
    }

    /// Get the thread identifier for the first thread in the list of queue threads.
    virtual id_type GetId() const noexcept
    {
        id_type no_id;
        return m_threads.empty() ? no_id : m_threads[0]->GetId();
    }

    /// This function is not used for containers of threads.
    std::thread* GetThread() override
    {
        return nullptr;
    }

    /// This function is not used for containers of threads.
    std::thread const* GetThread() const override
    {
        return nullptr;
    }

    /// Get the name of the thread.
    std::string const& GetName() const noexcept override
    {
        return m_name;
    }

    /// Start the thread.
    virtual void Start()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread>& t : m_threads)
            t->Start();
        m_started = true;
    }

    /// Return when the threaded function is a loop.
    bool IsThreadLoop() const noexcept override
    {
        return true;
    }

    bool IsTerminating() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        bool terminating = true;

        for (std::unique_ptr<QueueThread> const& t : m_threads)
        {
            if (!t->IsTerminating())
            {
                terminating = false;
                break;
            }
        }

        return terminating;
    }

    /// Terminate the thread, waiting until existing items in the queue are complete.
    void Terminate() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread>& t : m_threads)
            t->Terminate();
    }

    bool IsAborting() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        bool aborting = true;

        for (std::unique_ptr<QueueThread> const& t : m_threads)
        {
            if (!t->IsAborting())
            {
                aborting = false;
                break;
            }
        }

        return aborting;
    }

    /// Abort the thread, discarding any remaining items in the queue.
    void Abort() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread>& t : m_threads)
            t->Abort();
    }

    /// When not terminating and not aborting this will return true.
    bool IsContinuing() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        bool continuing = true;

        for (std::unique_ptr<QueueThread> const& t : m_threads)
        {
            if (t->IsAborting() || t->IsTerminating())
            {
                continuing = false;
                break;
            }
        }

        return continuing;
    }

    bool IsJoinable() const
    {
        bool joinable = false;

        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread> const& t : m_threads)
        {
            if (t->IsJoinable())
            {
                joinable = true;
                break;
            }
        }

        return joinable;
    }

    /// Join the thread back to the parent thread.
    void Join() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread>& t : m_threads)
            t->Join();
    }

    /// Tell the thread to pause.
    void Pause() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread>& t : m_threads)
            t->Pause();
    }

    /// Tell the thread to resume, if paused.
    void Resume() override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread>& t : m_threads)
            t->Resume();
    }

    /// Return true is the thread is paused.
    bool IsPaused() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread> const& t : m_threads)
            if (t->IsPaused())
                return true;

        return false;
    }

    /// When the thread function or thread loop is running, this will be true.
    bool IsRunning() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread> const& t : m_threads)
            if (t->IsRunning())
                return true;

        return false;
    }

    /// When the thread has started and has not been joined, this will be true.
    bool IsActive() const override
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        for (std::unique_ptr<QueueThread> const& t : m_threads)
            if (t->IsActive())
                return true;

        return false;
    }

private:

    /// Add one of more new threads.
    void AddThreads(std::size_t count = 1)
    {
        std::size_t offset = m_threads.size() + 1U;

        for (std::size_t pos = 0U; pos < count; ++pos)
        {
            std::string thread_name = m_name + " " + std::to_string(pos + offset);

            std::unique_ptr<QueueThread> queue_thread(new QueueThread(thread_name));

            if (m_started)
                queue_thread->Start();

            m_threads.push_back(std::move(queue_thread));
        }
    }

    /// Remove one of more threads.
    void RemoveThreads(std::size_t count = 1, bool terminate = true)
    {
        if (count > m_threads.size())
            count = m_threads.size();
        if (count > 0)
        {
            std::size_t start = count < m_threads.size() ? m_threads.size() - count : 0;
            for (std::size_t index = start; index < m_threads.size(); ++index)
            {
                if (terminate)
                    m_threads[index]->Terminate();
                else
                    m_threads[index]->Abort();
            }
            m_threads.resize(m_threads.size() - count);
        }
    }

    /// Find the next index to add to the queues.
    /// This function is not thread safe and requires caller to use mutex.
    std::size_t GetAddIndex() const
    {
        std::size_t index = 0;
        std::size_t smallest = ~static_cast<std::size_t>(0);

        for (std::size_t pos = 0; pos < m_threads.size(); ++pos)
        {
            std::unique_ptr<QueueThread> const& queue_thread = m_threads[pos];
            std::size_t size = queue_thread->GetSize();
            if (size < smallest)
            {
                index = pos;
                smallest = size;
            }
        }

        return index;
    }

private:

    /// Name of thread.
    std::string m_name;

    std::size_t m_max_queues;
    bool m_started;
    std::vector<std::unique_ptr<QueueThread>> m_threads;

    /// General use mutex for protecting access to multiple member variables.
    mutable std::mutex m_mutex;
};

} // namespace ocl

#endif // CPPOCL_GUARD_QUEUETHREADS_HPP
