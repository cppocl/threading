/*
Copyright 2022 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef CPPOCL_GUARD_QUEUETHREAD_HPP
#define CPPOCL_GUARD_QUEUETHREAD_HPP

#include "IThread.hpp"
#include "IFunctor.hpp"
#include "ThreadSafeQueue.hpp"
#include "utility/PlatformThreadCreate.hpp"
#include <mutex>
#include <atomic>
#include <exception>
#include <condition_variable>
#include <memory>
#include <string>

namespace ocl
{

// Thread class containing two threads to push and pop from a thread-safe queue of IFunctor objects.
//
// Guarantees that IFunctor objects can be pushed onto a queue and immediately returned,
// without waiting for the queue to process existing items.
class QueueThread final : public IThread
{
public:
    /// Construct a named QueueThread object.
    QueueThread(std::string const& name = "QueueThread")
        : m_name(name)
    {
        m_platform_thread = PlatformThreadCreate();
    }

    /// Move the QueueThread object.
    /// There is the potential for std::queue constructor to throw an exception,
    /// is it does not specify noexcept.
    QueueThread(QueueThread&& queue_thread) noexcept
    {
        try
        {
            queue_thread.m_mutex.lock();
            m_name = std::move(queue_thread.m_name);
            m_abort = queue_thread.m_abort.load();
            m_terminate = queue_thread.m_terminate.load();
            m_paused = queue_thread.m_paused.load();
            m_started = queue_thread.m_started.load();
            m_running = queue_thread.m_running.load();
            m_queue = std::move(queue_thread.m_queue);
            queue_thread.m_mutex.unlock();
        }
        catch (std::exception&)
        {
        }
    }

    QueueThread(QueueThread const&) = delete;
    QueueThread& operator=(QueueThread const&) = delete;
    QueueThread& operator=(QueueThread&&) = delete;

    ~QueueThread() override
    {
    }

    void Add(IFunctor* functor)
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            std::unique_ptr<IFunctor> p(functor);
            if (!m_abort && !m_terminate)
                m_queue.Push(std::move(p));
        }

        m_cv.notify_one();
    }

// Implement IThread interface.
public:

    /// This is always a child thread.
    bool IsMainThread() const noexcept override
    {
        return false;
    }

    /// Get the thread identifier.
    virtual id_type GetId() const noexcept
    {
        return m_thread.get_id();
    }

    /// Get the thread or nullptr if the thread is not running.
    std::thread* GetThread() noexcept override
    {
        return &m_thread;
    }

    /// Get the thread or nullptr if the thread is not running for const member functions.
    std::thread const* GetThread() const noexcept override
    {
        return &m_thread;
    }

    /// Get the name of the thread.
    std::string const& GetName() const noexcept override
    {
        return m_name;
    }

    /// Start the thread.
    virtual void Start()
    {
        m_thread = std::thread(&ThreadFunction, std::ref(*this));
        m_platform_thread->SetThreadName(m_thread, m_name.c_str());
        m_started = true;
    }

    /// Return when the threaded function is a loop.
    bool IsThreadLoop() const noexcept override
    {
        return true;
    }

    /// Return true when the thread is terminating.
    bool IsTerminating() const override
    {
        return m_terminate;
    }

    /// Terminate the thread, waiting until existing items in the queue are complete.
    void Terminate() override
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_terminate = true;
        }

        m_cv.notify_one();
    }

    /// Return true when the thread is aborting.
    bool IsAborting() const override
    {
        return m_abort;
    }

    /// Abort the thread, discarding any remaining items in the queue.
    void Abort() override
    {
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            m_abort = true;
        }

        m_cv.notify_one();
    }

    /// When not terminating and not aborting this will return true.
    bool IsContinuing() const override
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return !m_abort && !m_terminate;
    }

    bool IsJoinable() const noexcept override
    {
        return m_thread.joinable();
    }

    /// Join the thread back to the parent thread.
    void Join() override
    {
        m_thread.join();
        m_started = false;
    }

    /// Tell the thread to pause.
    void Pause() override
    {
        m_paused = true;
    }

    /// Tell the thread to resume, if paused.
    void Resume() override
    {
        m_paused = false;
    }

    /// Return true is the thread is paused.
    bool IsPaused() const noexcept override
    {
        return m_paused;
    }

    /// When the thread function or thread loop is running, this will be true.
    bool IsRunning() const noexcept override
    {
        return m_running;
    }

    /// When the thread has started and has not been joined, this will be true.
    bool IsActive() const noexcept override
    {
        return m_started;
    }

    /// Get the size of the queue.
    std::size_t GetSize() const
    {
        return m_queue.GetSize();
    }

private:
    void WaitToContinue()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_cv.wait(lock, [&] { return m_abort || m_terminate || !m_queue.IsEmpty(); });
    }

    /// The queue function that keeps looping until aborted or terminated.
    void ThreadLoop()
    {
        std::unique_ptr<IFunctor> functor;
        m_running = true;

        while (!m_abort && !m_terminate)
        {
            WaitToContinue();

            if (m_queue.Pop(functor))
                functor->Execute();
        }

        if (m_terminate)
        {
            std::unique_lock<std::mutex> lock(m_mutex);
            while (m_queue.Pop(functor))
                functor->Execute();
        }

        m_running = false;
    }

private:
    /// The function running in the thread.
    static void ThreadFunction(QueueThread& queue_thread)
    {
        queue_thread.ThreadLoop();
    }

private:
    /// Name of thread.
    std::string m_name;

    /// Thread state variables.
    std::atomic_bool m_abort = false;     // True after Abort function is called.
    std::atomic_bool m_terminate = false; // True after Terminate function called.
    std::atomic_bool m_paused = false;    // True when user pauses thread.
    std::atomic_bool m_started = false;   // True when the Start function is called and false after join.
    std::atomic_bool m_running = false;   // True while the thread loop is running.

    /// Queue of functors to be popped and executed in the thread loop.
    ThreadSafeQueue<std::unique_ptr<IFunctor>> m_queue;

    /// General use mutex for protecting access to multiple member variables.
    mutable std::mutex m_mutex;

    /// Condition variable used to release thread resources when not active.
    std::condition_variable m_cv;

    /// Must be defined last for other variables to be usable within the thread.
    std::thread m_thread;

    // Platform specific extensions supported through PlatformThread class.
    std::unique_ptr<PlatformThread> m_platform_thread;
};

} // namespace ocl

#endif // CPPOCL_GUARD_QUEUETHREAD_HPP
